package sera.com.hanita.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Publication {

    @SerializedName("id")
    @Expose
    private long public_id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("dateCreate_timestamp")
    @Expose
    private String dateCreateTimestamp;
    @SerializedName("dateUpdate_timestamp")
    @Expose
    private String dateUpdateTimestamp;

    public String getDateCreateTimestamp() {
        return dateCreateTimestamp;
    }

    public void setDateCreateTimestamp(String dateCreateTimestamp) {
        this.dateCreateTimestamp = dateCreateTimestamp;
    }

    public String getDateUpdateTimestamp() {
        return dateUpdateTimestamp;
    }

    public void setDateUpdateTimestamp(String dateUpdateTimestamp) {
        this.dateUpdateTimestamp = dateUpdateTimestamp;
    }

    public long getPublic_id() {
        return public_id;
    }

    public void setPublic_id(long public_id) {
        this.public_id = public_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
