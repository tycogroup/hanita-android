package sera.com.hanita.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification {

    @SerializedName("id")
    @Expose
    private String notify_id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("dateCreate_timestamp")
    @Expose
    private String dateCreateTimestamp;
    @SerializedName("dateUpdate_timestamp")
    @Expose
    private String dateUpdateTimestamp;

    public String getNotify_id() {
        return notify_id;
    }

    public void setNotify_id(String notify_id) {
        this.notify_id = notify_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDateCreateTimestamp() {
        return dateCreateTimestamp;
    }

    public void setDateCreateTimestamp(String dateCreateTimestamp) {
        this.dateCreateTimestamp = dateCreateTimestamp;
    }

    public String getDateUpdateTimestamp() {
        return dateUpdateTimestamp;
    }

    public void setDateUpdateTimestamp(String dateUpdateTimestamp) {
        this.dateUpdateTimestamp = dateUpdateTimestamp;
    }
}
