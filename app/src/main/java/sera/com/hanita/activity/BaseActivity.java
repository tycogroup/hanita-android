package sera.com.hanita.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import sera.com.hanita.R;
import sera.com.hanita.dialog.HanitaDialog;
import sera.com.hanita.fragment.CataractListFragment;
import sera.com.hanita.fragment.CompanyProfileFragment;
import sera.com.hanita.fragment.LensInstructionFragment;
import sera.com.hanita.fragment.PublicationsFragment;
import sera.com.hanita.fragment.SurgerieFragment;
import sera.com.hanita.fragment.VideoFragmentNew;
import sera.com.hanita.fragment.WebViewFragment;
import sera.com.hanita.fragment.cataract_products_child_fragments.CataractProductsCommonFragment;
import sera.com.hanita.object.Notification;
import sera.com.hanita.object.Publication;
import sera.com.hanita.utils.Utils;

public abstract class BaseActivity extends AppCompatActivity {

    public static final int LENS_POSITION = 0;
    public static final int COMPANY_PROFILE_POSITION = 1;
    public static final int CATARACT_PRODUCTS = 2;
    public static final int PUBLICATIONS = 3;
    public static final int TORIC_IOL_CALCULATOR = 4;
    public static final int SURGERIES = 5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Log.d("Hanita_LOG", "Permission is not granted, requesting");
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
        } else {
            Log.d("Hanita_LOG", "Permission is granted");
        }

    }

    public void showChildFragmentWithPosition(int position) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.new_slide_right, R.animator.new_slide_left, R.animator.new_slide_right, R.animator.new_slide_left)
                .add(R.id.container, CataractProductsCommonFragment.newInstance(position))
                .addToBackStack("CataractProductsCommonFragment")
                .commit();
    }

    public void showChildFragmentWithFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.new_slide_right, R.animator.new_slide_left, R.animator.new_slide_right, R.animator.new_slide_left)
                .add(R.id.container, fragment)
                .addToBackStack(fragment.getTag())
                .commit();
    }

    public void addFragments(int position) {
        switch (position) {
            case LENS_POSITION:
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.animator.new_slide_right, R.animator.new_slide_left, R.animator.new_slide_right, R.animator.new_slide_left)
                        .add(R.id.container, new LensInstructionFragment())
                        .addToBackStack("LensInstructionFragment")
                        .commit();
                break;
            case COMPANY_PROFILE_POSITION:
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.animator.new_slide_right, R.animator.new_slide_left, R.animator.new_slide_right, R.animator.new_slide_left)
                        .add(R.id.container, new CompanyProfileFragment())
                        .addToBackStack("CompanyProfileFragment")
                        .commit();
                break;
            case CATARACT_PRODUCTS:
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.animator.new_slide_right, R.animator.new_slide_left, R.animator.new_slide_right, R.animator.new_slide_left)
                        .add(R.id.container, new CataractListFragment(), CataractListFragment.TAG)
                        .addToBackStack("CataractListFragment")
                        .commit();
                break;
            case PUBLICATIONS:
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.animator.new_slide_right, R.animator.new_slide_left, R.animator.new_slide_right, R.animator.new_slide_left)
                        .add(R.id.container, new PublicationsFragment())
                        .addToBackStack("PublicationsFragment")
                        .commit();
                break;
            case TORIC_IOL_CALCULATOR:
                if (Utils.checkInternetConnection(this)) {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.animator.new_slide_right, R.animator.new_slide_left, R.animator.new_slide_right, R.animator.new_slide_left)
                            .add(R.id.container, new WebViewFragment())
                            .addToBackStack("WebViewFragment")
                            .commit();
                } else {

                    HanitaDialog hanitaAlertDialog = HanitaDialog.newInstance("No Internet connection", false, HanitaDialog.ALERT);
                    hanitaAlertDialog.setListener(new HanitaDialog.HanitaDialogDismissListener() {
                        @Override
                        public void onDismiss() {
                            //nothing
                        }
                    });
                    hanitaAlertDialog.show(getSupportFragmentManager(), "HanitaAlertDialog");
                }
                break;
            case SURGERIES:
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.animator.new_slide_right, R.animator.new_slide_left, R.animator.new_slide_right, R.animator.new_slide_left)
                        .add(R.id.container, new SurgerieFragment())
                        .addToBackStack("SurgerieFragment")
                        .commit();
                break;
        }
    }

    public void showFragmentVideo(String videoFile) {
        if (videoFile == null) {
            HanitaDialog alert = HanitaDialog.newInstance("error, no file found", false, HanitaDialog.ALERT);
            alert.show(getSupportFragmentManager(), "alert");
            return;
        }
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.new_slide_up, R.animator.new_slide_down, R.animator.new_slide_up, R.animator.new_slide_down)
                .add(R.id.container, VideoFragmentNew.newInstance(videoFile), VideoFragmentNew.TAG)
                .addToBackStack("VideoFragment")
                .commit();
    }

    public void showWebViewFragment(Notification notification, Publication publication) {
        if (notification != null) {
            if (Utils.checkInternetConnection(this) && !notification.getLink().equals("")) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.animator.new_slide_right, R.animator.new_slide_left, R.animator.new_slide_right, R.animator.new_slide_left)
                        .add(R.id.container, WebViewFragment.newInstance(notification))
                        .addToBackStack("WebViewFragment")
                        .commit();
            } else {
                showErrorDialog();
            }
        } else {
            if (Utils.checkInternetConnection(this) && !publication.getLink().equals("")) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.animator.new_slide_right, R.animator.new_slide_left, R.animator.new_slide_right, R.animator.new_slide_left)
                        .add(R.id.container, WebViewFragment.newInstance(publication))
                        .addToBackStack("WebViewFragment")
                        .commit();
            } else {
                showErrorDialog();
            }
        }
    }

    private void showErrorDialog() {
        HanitaDialog hanitaAlertDialog = HanitaDialog.newInstance("No URL available", false, HanitaDialog.ALERT);
        hanitaAlertDialog.setListener(new HanitaDialog.HanitaDialogDismissListener() {
            @Override
            public void onDismiss() {
                //nothing
            }
        });
        hanitaAlertDialog.show(getSupportFragmentManager(), "HanitaAlertDialog");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("PLAYGROUND", "Permission has been granted");
            } else {
                Log.d("PLAYGROUND", "Permission has been denied or request cancelled");
            }
        }
    }

}
