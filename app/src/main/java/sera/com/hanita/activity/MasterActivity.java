package sera.com.hanita.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import sera.com.hanita.R;
import sera.com.hanita.adapter.SideListAdapter;
import sera.com.hanita.dialog.HanitaDialog;
import sera.com.hanita.fragment.MainPagerFragment;
import sera.com.hanita.object.Notification;
import sera.com.hanita.object.Publication;

public class MasterActivity extends BaseActivity implements DrawerLayout.DrawerListener {

    private DrawerLayout drawer_layout;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_master);
        generateView();
        setUpDrawer();
        generateSideListAdapter();
        showChildFragmentWithFragment(new MainPagerFragment());
    }

    private void generateView() {
        drawer_layout = findViewById(R.id.drawer_layout);
        recyclerView = findViewById(R.id.recyclerView);
    }

    private void generateSideListAdapter() {
        SideListAdapter sideListAdapter = new SideListAdapter(this, this);
        recyclerView.setAdapter(sideListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.hasFixedSize();
    }


    public void showChildFragment(int position) {
        closeDelayDrawer();
        showChildFragmentWithPosition(position);
    }

    public void showChildFragment(Fragment fragment) {
        closeDelayDrawer();
        showChildFragmentWithFragment(fragment);
    }

    public void addFragment(int position) {
        addFragments(position);
    }

    private void closeDelayDrawer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START, true);
                }
            }
        }, 600);
    }

    private void setUpDrawer() {
        drawer_layout.addDrawerListener(this);
    }


    public void showVideoFragment(String videoFile) {
        showFragmentVideo(videoFile);
    }

    public void openCloseDrawer() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START, true);
        } else {
            drawer_layout.openDrawer(Gravity.START, true);
        }
    }

    @Override
    public void onDrawerSlide(@NonNull View view, float v) {
        Log.d("", "");
    }

    @Override
    public void onDrawerOpened(@NonNull View view) {
        Log.d("", "");
    }

    @Override
    public void onDrawerClosed(@NonNull View view) {
        Log.d("", "");
    }

    @Override
    public void onDrawerStateChanged(int i) {
        Log.d("", "");
    }

    public void showPDF(Publication publication) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(publication.getLink())));
    }

    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
            return;
        }
        if (getSupportFragmentManager().findFragmentByTag("VideoFragment") != null) {
            videoFragmentOnBackPressed();
            return;
        }
        doBackPress();
    }

    public void videoFragmentOnBackPressed() {
        super.onBackPressed();
    }

    public void doBackPress() {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            HanitaDialog hanitaDialog = HanitaDialog.newInstance("Exit?", true, HanitaDialog.INFO);
            hanitaDialog.setListener(new HanitaDialog.HanitaDialogDismissListener() {
                @Override
                public void onDismiss() {
                    finish();
                }
            });
            hanitaDialog.show(getSupportFragmentManager(), "HanitaDialog");
        } else {
            super.onBackPressed();
        }
    }

    public void showWebFragment(Notification notification,Publication publication) {
        showWebViewFragment(notification,publication);
    }

    public void popUpFragment() {
        getSupportFragmentManager().popBackStack();
    }
}
