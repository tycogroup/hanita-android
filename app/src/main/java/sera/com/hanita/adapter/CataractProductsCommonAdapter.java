package sera.com.hanita.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import sera.com.hanita.R;
import sera.com.hanita.fragment.cataract_products_child_fragments.CataractProductsCommonFragment;

public class CataractProductsCommonAdapter extends RecyclerView.Adapter<CataractProductsCommonAdapter.ViewHolder> {


    private Context context;
    private List<Drawable> itemList;
    private List<String> names;
    private List<String> description;
    private CataractProductsCommonFragment parent;

    public CataractProductsCommonAdapter(Context context, List<Drawable> list, List<String> name, List<String> desc, CataractProductsCommonFragment parent) {
        this.context = context;
        this.parent = parent;
        names = new ArrayList<>(name);
        description = new ArrayList<>(desc);
        itemList = new ArrayList<>(list);
    }

    @NonNull
    @Override
    public CataractProductsCommonAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = mLayoutInflater.inflate(R.layout.common_single_line, parent, false);
        return new CataractProductsCommonAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CataractProductsCommonAdapter.ViewHolder holder, final int position) {
        if (itemList.get(position) == null) {
            holder.imageView.setVisibility(View.INVISIBLE);
        } else {
            holder.imageView.setVisibility(View.VISIBLE);
            holder.imageView.setImageDrawable(itemList.get(position));
        }
        holder.nameTV.setText(names.get(position));
        holder.bottomTV.setText(description.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.openNextChildFragment(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView nameTV;
        TextView bottomTV;

        public ViewHolder(final View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            nameTV = itemView.findViewById(R.id.nameTV);
            bottomTV = itemView.findViewById(R.id.bottomTV);
        }
    }
}
