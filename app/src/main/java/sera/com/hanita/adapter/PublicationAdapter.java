package sera.com.hanita.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import sera.com.hanita.R;
import sera.com.hanita.fragment.PublicationsFragment;
import sera.com.hanita.object.Publication;

public class PublicationAdapter extends RecyclerView.Adapter<PublicationAdapter.ViewHolder> {

    private Context context;
    private List<Publication> itemList;
    private PublicationsFragment parent;

    public PublicationAdapter(Context context, PublicationsFragment parent) {
        this.context = context;
        this.parent = parent;
        itemList = new ArrayList<>();
    }

    @NonNull
    @Override
    public PublicationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = mLayoutInflater.inflate(R.layout.publication_adapter_single_line, parent, false);
        return new PublicationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PublicationAdapter.ViewHolder holder, final int position) {
        Picasso.with(context)
                .load(itemList.get(position).getImg())
                .noFade()
                .into(holder.image);
        holder.top_txt.setText(itemList.get(position).getTitle());
        holder.bottom_txt.setText(itemList.get(position).getBody());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //parent.showPDF(itemList.get(position));
                parent.showWebView(itemList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView top_txt;
        TextView bottom_txt;

        public ViewHolder(final View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            top_txt = itemView.findViewById(R.id.top_txt);
            bottom_txt = itemView.findViewById(R.id.bottom_txt);
        }

    }

    public void addItems(List<Publication> list) {
        itemList.addAll(list);
        notifyDataSetChanged();
    }

}
