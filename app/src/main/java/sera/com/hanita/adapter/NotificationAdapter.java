package sera.com.hanita.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sera.com.hanita.R;
import sera.com.hanita.fragment.NotificationFragment;
import sera.com.hanita.object.Notification;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private Context context;
    private List<Notification> itemList;
    private NotificationFragment parent;

    public NotificationAdapter(Context context, NotificationFragment parent) {
        this.context = context;
        this.parent = parent;
        itemList = new ArrayList<>();
    }

    @NonNull
    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = mLayoutInflater.inflate(R.layout.notification_adapter_single_line, parent, false);
        return new NotificationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final NotificationAdapter.ViewHolder holder, final int position) {
        holder.bind(position);
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.onClick(v);
            }
        });
    }

    public Notification getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView top_txt;
        TextView bottom_txt;

        public ViewHolder(final View itemView) {
            super(itemView);
            top_txt = itemView.findViewById(R.id.top_txt);
            bottom_txt = itemView.findViewById(R.id.bottom_txt);
        }

        public void bind(int position) {
            top_txt.setText(itemList.get(position).getTitle());
            bottom_txt.setText(itemList.get(position).getBody());
        }
    }

    public void addItems(List<Notification> list) {
        itemList.addAll(list);
        notifyDataSetChanged();
    }

}
