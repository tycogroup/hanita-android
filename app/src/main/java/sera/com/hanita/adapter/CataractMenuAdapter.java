package sera.com.hanita.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sera.com.hanita.R;
import sera.com.hanita.fragment.CataractListFragment;

public class CataractMenuAdapter extends RecyclerView.Adapter<CataractMenuAdapter.ViewHolder> {

    private Context context;
    private List<Drawable> itemList;
    private CataractListFragment parent;

    public CataractMenuAdapter(Context context, List<Drawable> list, CataractListFragment parent) {
        this.context = context;
        this.parent = parent;
        itemList = new ArrayList<>(list);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = mLayoutInflater.inflate(R.layout.cataract_menu_single_line, parent, false);
        return new CataractMenuAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.imageView.setImageDrawable(itemList.get(position));
        //holder.textView.setText(itemList.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.showFragment(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        //TextView textView;

        public ViewHolder(final View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            //textView = itemView.findViewById(R.id.textView);
        }
    }

}
