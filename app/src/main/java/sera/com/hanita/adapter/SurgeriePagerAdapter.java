package sera.com.hanita.adapter;

import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.utils.Constants;

public class SurgeriePagerAdapter extends PagerAdapter {

    private MasterActivity masterActivity;
    private List<Drawable> imageList;
    private long mLastClickTime = 0;

    public SurgeriePagerAdapter(MasterActivity masterActivity, List<Drawable> imageList) {
        this.masterActivity = masterActivity;
        this.imageList = imageList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = LayoutInflater.from(masterActivity).inflate(R.layout.surgerie_pager_item, container, false);
        ImageView imageView = itemView.findViewById(R.id.imageView);
        imageView.setImageDrawable(imageList.get(position));
        imageView.setOnClickListener(new SurgeriePagerAdapter.ObjectClickListener(position));
        container.addView(itemView);
        return itemView;
    }

    @Override
    public int getCount() {
        return imageList.size();
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public float getPageWidth(int position) {
        return 0.5f;
    }


    private class ObjectClickListener implements View.OnClickListener {
        int position;

        ObjectClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            masterActivity.showVideoFragment(getVideo(position));
        }
    }

    private String getVideo(int position) {
        switch (position) {
            case 0:
                return Constants.ASSI_ANCHOR;
            case 1:
                return Constants.SEE_LENS_SHASHKI;
            case 2:
                return Constants.SEE_LENS_AF_PANDEY;
            case 3:
                return Constants.SEE_LENS_HP_SAVARESI;
            case 4:
                return Constants.SEE_LENS_HP_BELUCCI;
            case 5:
                return Constants.SEE_LENS_MF_PAPPARO;
            case 6:
                return Constants.VIS_TOR_SURG;
        }
        return null;
    }
}