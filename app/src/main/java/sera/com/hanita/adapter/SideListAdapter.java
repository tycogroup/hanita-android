package sera.com.hanita.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;

public class SideListAdapter extends RecyclerView.Adapter<SideListAdapter.ViewHolder> {

    private Context context;
    private List<String> itemList;
    private MasterActivity masterActivity;

    public SideListAdapter(Context context, MasterActivity masterActivity) {
        this.context = context;
        this.masterActivity = masterActivity;
        setUpItemList();
    }

    private void setUpItemList() {
        itemList = new ArrayList<>();
        itemList.add(context.getString(R.string.side_list_0));
        itemList.add(context.getString(R.string.side_list_1));
        itemList.add(context.getString(R.string.side_list_2));
        itemList.add(context.getString(R.string.side_list_3));
        itemList.add(context.getString(R.string.side_list_4));
        itemList.add(context.getString(R.string.side_list_5));
        itemList.add(context.getString(R.string.side_list_6));
        itemList.add(context.getString(R.string.side_list_7));
//        itemList.add(context.getString(R.string.side_list_8));
        itemList.add(context.getString(R.string.side_list_9));
    }

    @NonNull
    @Override
    public SideListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = mLayoutInflater.inflate(R.layout.side_list_single_line, parent, false);
        return new SideListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SideListAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.button.setText(itemList.get(position));
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 8) {
                    new OpenLocalPDF(context, Constants.CONSTANT_TABLE_PDF).execute();
                } else {
                    masterActivity.showChildFragment(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Button button;

        public ViewHolder(final View itemView) {
            super(itemView);
            button = itemView.findViewById(R.id.button);
        }
    }

}
