package sera.com.hanita.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sera.com.hanita.api.interfaces.HanitaServiceApi;

import static sera.com.hanita.api.interfaces.HanitaServiceApi.BASE_URL;

public class RetrofitInstance {

    private static Retrofit instance;

    public static Retrofit getRetrofitInstance() {
        if (instance == null) {
            synchronized (HanitaServiceApi.class) {
                Gson gson = new GsonBuilder()
                        .setLenient()
                        .create();
                instance = new retrofit2.Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();
            }
        }

        return instance;
    }

}
