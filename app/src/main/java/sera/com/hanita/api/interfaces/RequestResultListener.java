package sera.com.hanita.api.interfaces;

import java.util.List;

public interface RequestResultListener<T> {
    void onSuccess(List<T> result);

    void onError(Throwable e);
}
