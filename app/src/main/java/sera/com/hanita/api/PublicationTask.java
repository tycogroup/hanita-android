package sera.com.hanita.api;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sera.com.hanita.api.interfaces.HanitaServiceApi;
import sera.com.hanita.dialog.WaitDialog;
import sera.com.hanita.fragment.PublicationsFragment;
import sera.com.hanita.object.Publication;

@Deprecated
public class PublicationTask extends AsyncTask<String, Runnable, Boolean> {

    private WaitDialog progressDialog;
    private PublicationsFragment fragment;
    private Context context;

    public PublicationTask(Context context, PublicationsFragment fragment) {
        this.fragment = fragment;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new WaitDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected Boolean doInBackground(String... params) {
        HanitaServiceApi api = RetrofitInstance.getRetrofitInstance().create(HanitaServiceApi.class);
        Call<List<Publication>> publicationCall = api.getPublications(
                "",
                "",
                "",
                "",
                ""
        );
        publicationCall.enqueue(new Callback<List<Publication>>() {
            @Override
            public void onResponse(Call<List<Publication>> call, Response<List<Publication>> response) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                fragment.updateAdapter(response.body());
            }

            @Override
            public void onFailure(Call<List<Publication>> call, Throwable t) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                Log.d("", "");
            }
        });
//        @SuppressLint("HardwareIds")
//        Call<User> response = api.login(
//                Encrypter.encrypt("login"),
//                Encrypter.encrypt(params[0]),
//                Encrypter.encrypt(params[1]),
//                Encrypter.encrypt(params[2]),
//                Encrypter.encrypt(Utils.getApplicationName(startActivity)),
//                Encrypter.encrypt("ComaxSign"),
//                Encrypter.encrypt(android.provider.Settings.Secure.getString(startActivity.getContentResolver(), Settings.Secure.ANDROID_ID)));
//        response.enqueue(new Callback<User>() {
//            @Override
//            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
//                Log.d("", "");
//                progressDialog.dismiss();
//                startActivity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        startActivity.openNextActivity();
//                    }
//                });
//
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<User> call, Throwable throwable) {
//                Log.d("", "");
//                startActivity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(startActivity);
//                        builder.setTitle("Error")
//                                .setMessage("Not cool")
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//
//                                    }
//                                });
//                        final AlertDialog dialog = builder.create();
//                        dialog.show();
//                    }
//                });
//            }
//        });
        return null;
    }


    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
    }

}
