package sera.com.hanita.api.interfaces;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import sera.com.hanita.object.Notification;
import sera.com.hanita.object.Publication;

public interface HanitaServiceApi {

    String BASE_URL = "http://hanitalenses.jet.tyco.co.il/api/"; // ?id&pagination&limit&from&count

    @GET("publications.php/")
    Call<List<Publication>> getPublications(
            @Query("id") String id,
            @Query("pagination") String pagination,
            @Query("limit") String limit,
            @Query("from") String from,
            @Query("count") String count
    );

    @GET("notifications.php/")
    Call<List<Notification>> getNotifications(
            @Query("id") String id,
            @Query("pagination") String pagination,
            @Query("limit") String limit,
            @Query("from") String from,
            @Query("count") String count
    );
}
