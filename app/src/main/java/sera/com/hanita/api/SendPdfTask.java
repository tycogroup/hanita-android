package sera.com.hanita.api;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import sera.com.hanita.BuildConfig;

public class SendPdfTask extends AsyncTask<String, Void, List<File>> {

    private WeakReference<Context> contextWeakReference;

    public SendPdfTask(Context context) {
        this.contextWeakReference = new WeakReference<>(context);
    }

    @Override
    protected List<File> doInBackground(String... params) {

        Context context = contextWeakReference.get();
        String appDirectoryName = BuildConfig.APPLICATION_ID;
        File fileRoot = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), appDirectoryName);
        AssetManager assetManager = context.getAssets();
        List<File> fileList = new ArrayList<>();
        List<File> result = new ArrayList<>();

        if (params != null) {
            for (String param : params) {
                fileList.add(new File(fileRoot, param + ".pdf"));
            }
        }

        InputStream in = null;
        OutputStream out = null;
        for (File file : fileList) {
            try {

                file.mkdirs();

                if (file.exists()) {
                    file.delete();
                }

                file.createNewFile();

                in = assetManager.open(file.getName());
                out = new FileOutputStream(file);
                copyFile(in, out);

                in.close();

                out.flush();
                out.close();

                result.add(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    @Override
    protected void onPostExecute(List<File> file) {
        super.onPostExecute(file);
        try {
            Context context = contextWeakReference.get();
            Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE); //ACTION_SEND
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Hanita PDF");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Please see attachment");
            ArrayList<Uri> uris = new ArrayList<>();
            for (int i = 0; i < file.size(); i++) {
                uris.add(Uri.fromFile(file.get(i)));
            }
            emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
            context.startActivity(Intent.createChooser(emailIntent, "Please choose method"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
