package sera.com.hanita.api;

import android.content.Context;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sera.com.hanita.api.interfaces.HanitaServiceApi;
import sera.com.hanita.api.interfaces.RequestResultListener;
import sera.com.hanita.dialog.WaitDialog;
import sera.com.hanita.object.Notification;
import sera.com.hanita.object.Publication;

public class NetworkManager {

    private HanitaServiceApi api;
    private WaitDialog dialog;

    public NetworkManager(Context context) {
        this.api = RetrofitInstance.getRetrofitInstance().create(HanitaServiceApi.class);
        dialog = new WaitDialog(context);
        dialog.setCancelable(false);
        dialog.show();
    }

    public void getPublications(final RequestResultListener<Publication> listener) {
        Call<List<Publication>> publicationCall = api.getPublications(
                "",
                "",
                "",
                "",
                ""
        );
        publicationCall.enqueue(new Callback<List<Publication>>() {
            @Override
            public void onResponse(Call<List<Publication>> call, Response<List<Publication>> response) {
                dialog.dismiss();
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Publication>> call, Throwable t) {
                dialog.dismiss();
                listener.onError(t);
            }
        });
    }


    public void getNotifications(final RequestResultListener<Notification> listener) {
        Call<List<Notification>> notificationsCall = api.getNotifications(
                "",
                "",
                "",
                "",
                ""
        );
        notificationsCall.enqueue(new Callback<List<Notification>>() {
            @Override
            public void onResponse(Call<List<Notification>> call, Response<List<Notification>> response) {
                dialog.dismiss();
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Notification>> call, Throwable t) {
                dialog.dismiss();
                listener.onError(t);
            }
        });
    }
    
}
