package sera.com.hanita.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Objects;

import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.R;
import sera.com.hanita.fragment.cataract_products_child_fragments.SeeLens;
import sera.com.hanita.utils.Constants;

public class CompanyProfileFragment extends Fragment implements View.OnClickListener {
    private Toolbar toolbar;
    private MasterActivity masterActivity;
    private TextView backBtn;
    private Button company_video_btn;
    private Button btn_4E_video;
    private TextView main_txt;
    private ImageView envelope;
    private ImageView list_icon;
    public CompanyProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_company_profile, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();

        String htmlString =
                "<p><h1><font color='#27aae1'>Hanita Lenses - A Trusted Company for Intraocular Lens Solutions</font></h1><br>"
                        + "Hanita Lenses is a worldwide trusted manufacturer and provider of intraocular lens solutions for cataract surgery. With more than 30 years of experience in meeting the varied needs of ophthalmic surgeons, the Hanita Lenses name is synonymous with high quality, reliability, and service.\n<br><br>"
                        + "Hanita Lenses, founded in 1981, offers cataract surgeons a complete solution for proven vision correction. Based on high-quality intraocular lenses, the company’s solutions are backed by responsive service and access to Hanita’s expert knowledge base. Its main ophthalmology solutions, lead by Hanita Lenses’ precision-designed platforms SeeLens and BunnyLens, are targeted to cataract surgery and have additional applications in the emerging field of refractive lens exchange.<br><br>"
                        + "In addition to developing, manufacturing and marketing Hanita Lenses’ branded intraocular lens solutions, the company works in partnership with ophthalmologists, researchers and other companies to develop and manufacture new and innovative products, both directly and under OEM agreements. The company also manufactures a line of in-house developed contact lenses, marketed under various brand names.<br><br>"
                        + "With its ongoing investment in R&amp;D and its expert team, Hanita is able to draw on a rich knowledge base to support its customers and partners. The company’s internal resources are augmented by an international advisory board comprising leading ophthalmology experts from top medical centers and universities, creating an extensive network of knowledge leadership.<br><br>"
                        + "Partnership as a core value<br>"
                        + "As a well-established company that specializes in intraocular lens solutions, Hanita is unique in combining expert knowledge and world-class capabilities with easy flexibility and fast responsiveness.<br><br>"
                        + "At Hanita Lenses, customers are viewed as partners united in the common goal of ensuring the best possible outcomes for patients. To this end, Hanita is totally committed to designing and delivering premium products and services that support optimal results for patients and provide maximum ease of use and convenience to surgeons and medical staff. Customers can turn to the company as a knowledge base as well as for patient information and other practice support.<br><br>"
                        + "Proven safety, quality and reliability<br><br>"
                        + "All Hanita Lenses intraocular lenses are developed in the company’s state-of-the-art labs and manufactured to the highest quality standards utilizing advance optical equipment. The company’s facilities and processes carry ISO 13485:2003 and ISO 9001: 2000 certifications.<br><br>"
                        + "Hanita Lenses products are CE approved and, in addition, have regulatory approvals in China, Russia, India, Latin America and Eastern Europe countries. Approvals are pending in other jurisdictions, including South Korea, and the Philippines. The company follows a strict professional code of ethics and is unrelenting in its constant attention to quality"
                        + "</p>";

        main_txt.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
        return v;
    }

    private void initListeners() {
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        backBtn.setOnClickListener(this);
        company_video_btn.setOnClickListener(this);
        btn_4E_video.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }

    private void initView(View v) {
        toolbar = v.findViewById(R.id.toolbar);
        backBtn = v.findViewById(R.id.backBtn);
        main_txt = v.findViewById(R.id.main_txt);
        company_video_btn = v.findViewById(R.id.company_video_btn);
        btn_4E_video = v.findViewById(R.id.btn_4E_video);
        envelope = v.findViewById(R.id.envelope);
        list_icon = v.findViewById(R.id.list_icon);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.backBtn:
                masterActivity.onBackPressed();
                break;
            case R.id.company_video_btn:
                masterActivity.showVideoFragment(Constants.COMPANY_PROFILE_SLIDE_1);
                break;
            case R.id.btn_4E_video:
                masterActivity.showVideoFragment(Constants.COMPANY_PROFILE_SLIDE_2);
                break;
        }
    }
}
