package sera.com.hanita.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.adapter.PublicationAdapter;
import sera.com.hanita.api.NetworkManager;
import sera.com.hanita.api.interfaces.RequestResultListener;
import sera.com.hanita.object.Publication;

/**
 * A simple {@link Fragment} subclass.
 */
public class PublicationsFragment extends Fragment implements View.OnClickListener {

    private TextView backBtn;
    private MasterActivity masterActivity;
    private RecyclerView recyclerView;
    private PublicationAdapter publicationAdapter;
    private ImageView envelope;
    private ImageView list_icon;
//    private RelativeLayout relative_0;
//    private RelativeLayout relative_1;
//    private RelativeLayout relative_2;
//    private RelativeLayout relative_3;

    public PublicationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_publications_new, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();

        publicationAdapter = new PublicationAdapter(masterActivity, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(masterActivity));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(publicationAdapter);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        callServer();
//        new PublicationTask(masterActivity, this).execute();
    }

    private void initView(View v) {
        backBtn = v.findViewById(R.id.backBtn);
        recyclerView = v.findViewById(R.id.recyclerView);
        envelope = v.findViewById(R.id.envelope);
        list_icon = v.findViewById(R.id.list_icon);
//        relative_0 = v.findViewById(R.id.relative_0);
//        relative_1 = v.findViewById(R.id.relative_1);
//        relative_2 = v.findViewById(R.id.relative_2);
//        relative_3 = v.findViewById(R.id.relative_3);
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
//        relative_0.setOnClickListener(this);
//        relative_1.setOnClickListener(this);
//        relative_2.setOnClickListener(this);
//        relative_3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
//            case R.id.relative_0:
//                new OpenLocalPDF(getContext(), Constants.PUBLICATION_0).execute();
//                break;
//            case R.id.relative_1:
//                new OpenLocalPDF(getContext(), Constants.PUBLICATION_1).execute();
//                break;
//            case R.id.relative_2:
//                new OpenLocalPDF(getContext(), Constants.PUBLICATION_2).execute();
//                break;
//            case R.id.relative_3:
//                new OpenLocalPDF(getContext(), Constants.PUBLICATION_3).execute();
//                break;
        }
    }

    private void callServer() {
        new NetworkManager(masterActivity).getPublications(new RequestResultListener<Publication>() {
            @Override
            public void onSuccess(List<Publication> result) {
                updateAdapter(result);
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(masterActivity, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void updateAdapter(List<Publication> list) {
        publicationAdapter.addItems(list);
    }

    public void showPDF(Publication publication) {
        masterActivity.showPDF(publication);
    }

    public void showWebView(Publication publication) {
        masterActivity.showWebFragment(null, publication);
    }
}
