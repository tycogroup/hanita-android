package sera.com.hanita.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.adapter.CataractMenuAdapter;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;
import sera.com.hanita.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class CataractListFragment extends Fragment implements View.OnClickListener {

    private RecyclerView recyclerView;
    private CataractMenuAdapter cataractMenuAdapter;
    private Toolbar toolbar;
    private TextView backBtn;
    private MasterActivity masterActivity;
    private ImageView envelope;
    private ImageView list_icon;
    public static String TAG = "CataractListFragment";

    public CataractListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_cataract_list_fragment, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();

        cataractMenuAdapter = new CataractMenuAdapter(getContext(), Utils.getImageList(Objects.requireNonNull(getContext())), this);
        recyclerView.setAdapter(cataractMenuAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
        recyclerView.hasFixedSize();

        return v;
    }

    private void initListeners() {
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        backBtn.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }

    private void initView(View v) {
        recyclerView = v.findViewById(R.id.recyclerView);
        toolbar = v.findViewById(R.id.toolbar);
        backBtn = v.findViewById(R.id.backBtn);
        envelope = v.findViewById(R.id.envelope);
        list_icon = v.findViewById(R.id.list_icon);
    }


    public void showFragment(int position) {
        if (position == 8) {
            new OpenLocalPDF(getContext(), Constants.CONSTANT_TABLE_PDF).execute();
        } else {
            masterActivity.showChildFragment(position);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
        }
    }
}
