package sera.com.hanita.fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.dialog.HanitaDialog;
import sera.com.hanita.dialog.WaitDialog;
import sera.com.hanita.object.Notification;
import sera.com.hanita.object.Publication;
import sera.com.hanita.utils.Constants;


/**
 * A simple {@link Fragment} subclass.
 */
public class WebViewFragment extends Fragment implements View.OnClickListener {


    private MasterActivity masterActivity;
    private TextView backBtn;
    private WebView browser;
    private WaitDialog waitDialog;
    private ImageView envelope;
    private ImageView list_icon;
    private String GoogleDocs = "http://docs.google.com/gview?embedded=true&url=";
    private static int mode = -1;

    public WebViewFragment() {
        // Required empty public constructor
    }

    public static WebViewFragment newInstance(Notification notification) {
        WebViewFragment f = new WebViewFragment();
        mode = Constants.NOTIFICATION_MODE;
        Bundle args = new Bundle();
        args.putString("link", notification.getLink());
        f.setArguments(args);
        return f;
    }

    public static WebViewFragment newInstance(Publication publication) {
        WebViewFragment f = new WebViewFragment();
        mode = Constants.PUBLICATION_MODE;
        Bundle args = new Bundle();
        args.putString("link", publication.getLink());
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_web_view, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initAndStartWebView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initAndStartWebView() {
        waitDialog.show();
        browser.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                if (url.contains("blank")) {
                    HanitaDialog hanitaAlertDialog = HanitaDialog.newInstance("URL are not available", false, HanitaDialog.ALERT);
                    hanitaAlertDialog.setListener(new HanitaDialog.HanitaDialogDismissListener() {
                        @Override
                        public void onDismiss() {
                            masterActivity.popUpFragment();
                        }
                    });
                    hanitaAlertDialog.show(masterActivity.getSupportFragmentManager(), "HanitaAlertDialog");
                }
                waitDialog.dismiss();
            }
        });
        browser.getSettings().setJavaScriptEnabled(true);
        browser.getSettings().setDomStorageEnabled(true);
        browser.setOverScrollMode(WebView.OVER_SCROLL_NEVER);
        if (getArguments() == null) {
            browser.loadUrl(Constants.WEB_VIEW_CALCULATOR_URL);
        } else {
            if (mode == Constants.NOTIFICATION_MODE) {
                browser.loadUrl(getArguments().getString("link"));
            } else {
                browser.loadUrl(GoogleDocs + getArguments().getString("link"));
            }
        }
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
        waitDialog = new WaitDialog(masterActivity);
    }

    private void initView(View v) {
        backBtn = v.findViewById(R.id.backBtn);
        browser = v.findViewById(R.id.webView);
        envelope = v.findViewById(R.id.envelope);
        list_icon = v.findViewById(R.id.list_icon);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
        }
    }

}
