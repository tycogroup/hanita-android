package sera.com.hanita.fragment.cataract_products_child_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.dialog.dialog_fragment.CommonPopUpDialog;
import sera.com.hanita.dialog.dialog_fragment.SendByEmailDialog;
import sera.com.hanita.fragment.NotificationFragment;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;

/**
 * A simple {@link Fragment} subclass.
 */
public class SeeLensHP extends Fragment implements View.OnClickListener {

    private TextView backBtn;
    private ImageView envelope;
    private MasterActivity masterActivity;
    private CommonPopUpDialog commonPopUpDialog;
    private ImageView plus_btn_1;
    private ImageView plus_btn_2;
    private ImageView plus_btn_3;
    private ImageView plus_btn_4;
    private ImageView plus_btn_5;
    private ImageView upload_icon;
    private ImageView list_icon;
    private ImageView tableImage;
    private TextView html_TV;
    private TextView tech_specs_btn;
    private String htmlString;
    private boolean showSpecs = true;
    private Button brochure_btn;
    private Button clinical_report_btn;
    private Button see_lens_HP_btn;

    public SeeLensHP() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_see_lens_h, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();

        htmlString =
                " <p><font color='red'>Attributes</font><br><br>"
                        + "Advanced Optical Design<br><br>"
                        + "The Aspheric SeeLens HP was designed using the most advanced tools by a professional R&amp;D team of optical and mechanical engineers.<br><br><br>"
                        + "The optical profile of the SeeLens HP was calculated using ZEMAX™ software – a simulating tool for the optical design optimization. Calculations were aimed to minimize all aberrations, including the spherical aberration of the cornea, and to optimize the MTF (Modulated Transfer Function).<br><br><br>"
                        + "<font color='red'> Eye Model</font><br><br>"
                        + "The Optical design of SeeLens HP was performed using the Arizona Eye Model[1]. The parameters and dimensions of the eye model are consistent with average human data. The model was designed to match clinical levels of aberration, both on and off axis. The retina curvature is designed to split the tangential and sagittal foci off-axis. The result is an accurate simulation of the visual performance of the SeeLens HP in a post-operative eye.<br><br>"
                        + "[1] Field Guide to Visual and Ophthalmic Optics; Jim Schwiegerling; Nov. 2004<br><br><br>"
                        + "<font color='red'>Geometrical Design</font><br><br>"
                        + "SeeLens HP ensures excellent stability and centration due to the unique C-loop mechanical design, ensuring wide angle of contact.<br><br>"
                        + "360° square edge in order to minimize PCO.<br><br><br>"
                        + "<font color='red'>Material</font><br><br>"
                        + "The SeeLens HP is made of a hydrophobic material with bio-adhesive characteristics, resulting in reduction of secondary cataract procedures [1].<br><br>"
                        + "The SeeLens HP has a very a very low glistening effect [2 3].<br><br>"
                        + "The SeeLens HP hydrophobic material incorporates a natural yellow, violet filtering, chromophore for better protection of the retina.<br><br>"
                        + "[1] Advances in Intraocular Lens Material and Designs: Maximizing Biocompatibility and Optical Performance; Ophthalmogica 2003;217<br><br>"
                        + "[2] Data from Benz Rechearch and Development QA Dept<br><br>"
                        + "[3] Glistening in the Single -Piece Hydrophobic, Acrylic Intraocular Lenses; Aaron Waite, Nathan Faulker and Randall J Olson; J Ophthalmol 2007; 144: 143- 144.<br><br>"
                        + "</p>";

        html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);

        return v;
    }

    private void initView(View v) {
        upload_icon = v.findViewById(R.id.upload_icon);
        backBtn = v.findViewById(R.id.backBtn);
        envelope = v.findViewById(R.id.envelope);
        html_TV = v.findViewById(R.id.html_TV);
        tech_specs_btn = v.findViewById(R.id.tech_specs_btn);
        plus_btn_1 = v.findViewById(R.id.plus_btn_1);
        plus_btn_2 = v.findViewById(R.id.plus_btn_2);
        plus_btn_3 = v.findViewById(R.id.plus_btn_3);
        plus_btn_4 = v.findViewById(R.id.plus_btn_4);
        plus_btn_5 = v.findViewById(R.id.plus_btn_5);
        brochure_btn = v.findViewById(R.id.brochure_btn);
        clinical_report_btn = v.findViewById(R.id.clinical_report_btn);
        see_lens_HP_btn = v.findViewById(R.id.see_lens_HP_btn);
        list_icon = v.findViewById(R.id.list_icon);
        tableImage = v.findViewById(R.id.tableImage);
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        tech_specs_btn.setOnClickListener(this);
        plus_btn_1.setOnClickListener(this);
        plus_btn_2.setOnClickListener(this);
        plus_btn_3.setOnClickListener(this);
        plus_btn_4.setOnClickListener(this);
        plus_btn_5.setOnClickListener(this);
        brochure_btn.setOnClickListener(this);
        clinical_report_btn.setOnClickListener(this);
        see_lens_HP_btn.setOnClickListener(this);
        upload_icon.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.upload_icon:
                SendByEmailDialog.newInstance(
                        Constants.SEE_LENS_HP_BROCHURE,
                        Constants.SEE_LENS_HP_CLINICAL,
                        Constants.SEE_LENS_HP_RAW_MATERIAL)
                        .show(masterActivity.getSupportFragmentManager(), "SendByEmailDialog");
                break;
            case R.id.brochure_btn:
                new OpenLocalPDF(getContext(), Constants.SEE_LENS_HP_BROCHURE).execute();
                break;
            case R.id.clinical_report_btn:
                new OpenLocalPDF(getContext(), Constants.SEE_LENS_HP_CLINICAL).execute();
                break;
            case R.id.see_lens_HP_btn:
                new OpenLocalPDF(getContext(), Constants.SEE_LENS_HP_RAW_MATERIAL).execute();
                break;
            case R.id.tech_specs_btn:
                if (showSpecs) {
                    showSpecs = false;
                    tech_specs_btn.setText(masterActivity.getString(R.string.back_txt));
                    html_TV.setVisibility(View.GONE);
                    tableImage.setVisibility(View.VISIBLE);
                } else {
                    showSpecs = true;
                    tech_specs_btn.setText(masterActivity.getString(R.string.tech_specs));
                    html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
                    html_TV.setVisibility(View.VISIBLE);
                    tableImage.setVisibility(View.GONE);
                }
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
            case R.id.plus_btn_1:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_SEE_LENNS_HP__HOT_SPOT_4);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_2:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_SEE_LENNS_HP__HOT_SPOT_5);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_3:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_SEE_LENNS_HP__HOT_SPOT_3);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_4:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_SEE_LENNS_HP__HOT_SPOT_2);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_5:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_SEE_LENNS_HP__HOT_SPOT_1);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
        }


    }


}
