package sera.com.hanita.fragment.cataract_products_child_fragments;


import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.dialog.dialog_fragment.CommonPopUpDialog;
import sera.com.hanita.dialog.dialog_fragment.SendByEmailDialog;
import sera.com.hanita.fragment.NotificationFragment;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;

/**
 * A simple {@link Fragment} subclass.
 */
public class HpEasy extends Fragment implements View.OnClickListener {

    private TextView backBtn;
    private TextView html_TV;
    private TextView tech_specs_btn;
    private MasterActivity masterActivity;
    private CommonPopUpDialog commonPopUpDialog;
    private ImageView plus_btn_1;
    private ImageView plus_btn_2;
    private ImageView plus_btn_3;
    private ImageView plus_btn_4;
    private ImageView plus_btn_5;
    private ImageView plus_btn_6;
    private ImageView upload_icon;
    private ImageView envelope;
    private ImageView list_icon;
    private ImageView tableImage;
    private boolean showSpecs = true;
    private String htmlString;

    private Button brochure_btn;
    private Button bunny_lenses_btn;
    private Button hp_easy_lenses_btn;

    public HpEasy() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_hp_easy, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();
        htmlString =
                "<p><font color='black'><b>Advanced optical design</b></font><br>"
                        + "The Aspheric HP lenses were designed using the most<br>"
                        + "advanced tools by a professional R&amp;D team of optical and<br>"
                        + "mechanical engineers. The optical profile of the HP lenses was<br>"
                        + "calculated using ZEMAX™ software – a simulating tool for the<br>"
                        + "optical design optimization. Calculations were aimed to<br>"
                        + "minimize all aberrations, including the spherical aberration of<br>"
                        + "the cornea, and to optimize the MTF (Modulated Transfer Function).<br><br>"
                        + "<font color='black'><b>Eye Model</b></font><br>"
                        + "The optical design of the HP lenses was performed using the Arizona Eye Model[1].<br>"
                        + "The parameters and dimensions of the eye model are consistent with average human data. The model<br>"
                        + "was designed to match clinical level of aberration, both on and off axis.<br>"
                        + "This result  is an accurate simulation of of the visual performance of the HP lense in a post-operative eye.<br><br>"
                        + "<font color='black'><b>[1] Field Guide to Visual Ophthalmic Optics; Jim Schwiegerling; Nov. 2004</b></font>" + "</p>";

        html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
        return v;
    }

    private void initView(View v) {
        upload_icon = v.findViewById(R.id.upload_icon);
        envelope = v.findViewById(R.id.envelope);
        backBtn = v.findViewById(R.id.backBtn);
        html_TV = v.findViewById(R.id.html_TV);
        tech_specs_btn = v.findViewById(R.id.tech_specs_btn);
        brochure_btn = v.findViewById(R.id.brochure_btn);
        bunny_lenses_btn = v.findViewById(R.id.bunny_lenses);
        hp_easy_lenses_btn = v.findViewById(R.id.hp_easy_lenses);
        plus_btn_1 = v.findViewById(R.id.plus_btn_1);
        plus_btn_2 = v.findViewById(R.id.plus_btn_2);
        plus_btn_3 = v.findViewById(R.id.plus_btn_3);
        plus_btn_4 = v.findViewById(R.id.plus_btn_4);
        plus_btn_5 = v.findViewById(R.id.plus_btn_5);
        plus_btn_6 = v.findViewById(R.id.plus_btn_6);
        list_icon = v.findViewById(R.id.list_icon);
        tableImage = v.findViewById(R.id.tableImage);
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        tech_specs_btn.setOnClickListener(this);
        plus_btn_1.setOnClickListener(this);
        plus_btn_2.setOnClickListener(this);
        plus_btn_3.setOnClickListener(this);
        plus_btn_4.setOnClickListener(this);
        plus_btn_5.setOnClickListener(this);
        plus_btn_6.setOnClickListener(this);
        brochure_btn.setOnClickListener(this);
        bunny_lenses_btn.setOnClickListener(this);
        hp_easy_lenses_btn.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
        upload_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.upload_icon:
                SendByEmailDialog.newInstance(
                        Constants.PRODUCT_BROCHURE_BLENS,
                        Constants.QA_BLENS,
                        Constants.CLONICAL_EXP,
                        Constants.CLONICAL_EXP_1Y)
                        .show(masterActivity.getSupportFragmentManager(), "SendByEmailDialog");
                break;
            case R.id.brochure_btn:
                new OpenLocalPDF(getContext(), Constants.HP_EASY_BROCHURE).execute();
                break;
            case R.id.bunny_lenses:
                masterActivity.showVideoFragment(Constants.HP_EASY_BUNNY_LENS);
                break;
            case R.id.hp_easy_lenses:
                masterActivity.showVideoFragment(Constants.HP_EASY_SEE_LENS);
                break;
            case R.id.tech_specs_btn:
                if (showSpecs) {
                    showSpecs = false;
                    tech_specs_btn.setText(masterActivity.getString(R.string.back_txt));
                    html_TV.setVisibility(View.GONE);
                    tableImage.setVisibility(View.VISIBLE);
                } else {
                    showSpecs = true;
                    tech_specs_btn.setText(masterActivity.getString(R.string.tech_specs));
                    html_TV.setVisibility(View.VISIBLE);
                    tableImage.setVisibility(View.GONE);
                }
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
            case R.id.plus_btn_1:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.HP_EASY_HOT_SPOT_1);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_2:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.HP_EASY_HOT_SPOT_2);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_3:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.HP_EASY_HOT_SPOT_3);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_4:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.HP_EASY_HOT_SPOT_4);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_5:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.HP_EASY_HOT_SPOT_5);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_6:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.HP_EASY_HOT_SPOT_6);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
        }
    }
}
