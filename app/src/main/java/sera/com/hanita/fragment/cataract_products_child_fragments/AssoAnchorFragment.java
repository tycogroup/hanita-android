package sera.com.hanita.fragment.cataract_products_child_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.api.SendPdfTask;
import sera.com.hanita.dialog.dialog_fragment.CommonPopUpDialog;
import sera.com.hanita.dialog.dialog_fragment.SendByEmailDialog;
import sera.com.hanita.fragment.NotificationFragment;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;

/**
 * A simple {@link Fragment} subclass.
 */
public class AssoAnchorFragment extends Fragment implements View.OnClickListener {

    private TextView main_txt;
    private TextView backBtn;
    private ImageView plus_btn_1;
    private ImageView plus_btn_2;
    private ImageView plus_btn_3;
    private ImageView plus_btn_4;
    private ImageView plus_btn_5;
    private ImageView upload_icon;
    private ImageView envelope;
    private ImageView list_icon;
    private CommonPopUpDialog commonPopUpDialog;
    private MasterActivity masterActivity;
    private Button brochure_btn;
    private Button zonular_support;

    public AssoAnchorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.assi_anchor_layout, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();
        String htmlString =
                "<p>Hanita Lenses novel capsular anchor device Securing the capsular bag to the sclerar wall.<br><br>"
                        + "Enables  cataract  surgery  for  subluxated  lens  Centers  subluxated  capsule  Provides  a  wide  contact  between  the  device  and  the  anterior  capsule  CE  approved<br><br>"
                        + "<font color='#00467e'>Technical Specifications</font><br>"
                        + "Material……………………  Blue tinted PMMA<br>"
                        + "Y.A.G laser………………..  Compatible<br>"
                        + "Centers subluxated capsule<br>"
                        + "Developed with Prof. E. Assia, Israel<br><br><br>"
                        + "<font color='#00467e'>Technical Specifications</font><br>"
                        + "* For  detailed  clinical  instructions  please  contact  <br><br>"
                        + "Hanita  Lenses<br><br>"
                        + "A  conventional phaco-incision  (min 2,5  mm)  is performed, followed  by  a  capsulorhexis.  Easy  AssiAnchor  insertion  and  fixation  using  a  suture.<br><br>"
                        + "The  AssiAnchor clips  to  the  capsule. The  capsule  is centered  and secured  to  the scleral  wall.<br><br>"
                        + "Phacoemulsification procedure  can  be easily  performed, followed  by  IOL implantation<br><br>"
                        + "</p>";


        main_txt.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
        return v;
    }

    private void initView(View v) {
        upload_icon = v.findViewById(R.id.upload_icon);
        envelope = v.findViewById(R.id.envelope);
        main_txt = v.findViewById(R.id.main_txt);
        backBtn = v.findViewById(R.id.backBtn);
        brochure_btn = v.findViewById(R.id.brochure_btn);
        zonular_support = v.findViewById(R.id.zonular_support);
        plus_btn_1 = v.findViewById(R.id.plus_btn_1);
        plus_btn_2 = v.findViewById(R.id.plus_btn_2);
        plus_btn_3 = v.findViewById(R.id.plus_btn_3);
        plus_btn_4 = v.findViewById(R.id.plus_btn_4);
        plus_btn_5 = v.findViewById(R.id.plus_btn_5);
        list_icon = v.findViewById(R.id.list_icon);
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        brochure_btn.setOnClickListener(this);
        zonular_support.setOnClickListener(this);
        upload_icon.setOnClickListener(this);
        envelope.setOnClickListener(this);
        plus_btn_1.setOnClickListener(this);
        plus_btn_2.setOnClickListener(this);
        plus_btn_3.setOnClickListener(this);
        plus_btn_4.setOnClickListener(this);
        plus_btn_5.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.upload_icon:
                SendByEmailDialog.newInstance(Constants.ZONULAR_ASSI, Constants.PRODUCT_BROCHURE_ASSI)
                        .show(masterActivity.getSupportFragmentManager(), "SendByEmailDialog");
                break;
            case R.id.zonular_support:
                new OpenLocalPDF(getContext(), Constants.ZONULAR_ASSI).execute();
                break;
            case R.id.brochure_btn:
                new OpenLocalPDF(getContext(), Constants.PRODUCT_BROCHURE_ASSI).execute();
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
            case R.id.plus_btn_1:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_ASSI_HOT_SPOT_1);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_2:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_ASSI_HOT_SPOT_2);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_3:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_ASSI_HOT_SPOT_3);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_4:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_ASSI_HOT_SPOT_4);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_5:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_ASSI_HOT_SPOT_5);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
        }
    }
}
