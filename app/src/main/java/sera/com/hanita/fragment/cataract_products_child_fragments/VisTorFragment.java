package sera.com.hanita.fragment.cataract_products_child_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.dialog.dialog_fragment.CommonPopUpDialog;
import sera.com.hanita.dialog.dialog_fragment.SendByEmailDialog;
import sera.com.hanita.fragment.NotificationFragment;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;

/**
 * A simple {@link Fragment} subclass.
 */
public class VisTorFragment extends Fragment implements View.OnClickListener {

    private MasterActivity masterActivity;
    private TextView backBtn;
    private TextView html_TV;
    private TextView tech_specs_btn;
    private ImageView plus_btn_1;
    private ImageView plus_btn_2;
    private ImageView plus_btn_3;
    private ImageView plus_btn_4;
    private ImageView plus_btn_5;
    private ImageView upload_icon;
    private ImageView list_icon;
    private ImageView tableImage;
    private CommonPopUpDialog commonPopUpDialog;
    private Button brochure_btn;
    private Button clinical_report_btn;
    private Button video;
    private String htmlString;
    private ImageView envelope;
    private boolean showSpecs = true;
    public VisTorFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_vis_tor, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();

        htmlString =
                "<p><font color='#884c0d'> Advanced Optical Design</font><br>"
                        + "Leveraging Hanita Lenses’ decades long experience in developing advanced IOLs, VisTor’s optical design is based on Hanita Lenses’ clinically proven aspheric IOL. The optical profile of the VisTor lens was calculated using ZEMAX™ simulation software in order to minimize all aberrations, including the spherical aberration of the cornea, and to optimize the modulation Transfer Function (MTF).<br><br>"
                        + "The optical design of the VisTor lenses utilized the advanced Arizona Eye model (1), the premier model for toric lens design due to its close correspondence to clinical level of aberrations, both on and off-axis. In this eye model, unlike others, the retina curvature is designed to split the tangential and sagittal foci off-axis, providing an optimal platform for follow an apodization function, meaning they reduce in height according to an algorithm designed so the patient can function comfortably all day long.<br>"
                        + "The aspheric surface integrated with the diffractive profile is the BunnyLens AF aberration free optic, which has been clinically proven to provide excellent vision in all conditions.<br>"
                        + "The state-of-the-art apodized diffractive surface provides visual functionality throughout the day:<br>"
                        + "In photopic conditions, when the pupil is small, the BunnyLens MF distributes 65% of the light for far vision and 35% for near vision, providing light for all daytime activities.<br>"
                        + "follow an apodization function, meaning they reduce in height according to an algorithm designed so the patient can function comfortably all day long.<br>"
                        + "The aspheric surface integrated with the diffractive profile is the SeeLens AF aberration free optic, which has been clinically proven to provide excellent vision in all conditions.<br>"
                        + "The state-of-the-art apodized diffractive surface provides visual functionality throughout the day:<br>"
                        + "•\tIn photopic conditions, when the pupil is small, the SeeLens MF distributes 65% of the light for far vision and 35% for near vision, providing light for all daytime activities.<br>"
                        + "•\tIn mesopic and scotopic conditions, distance vision gets most of the light energy, enabling night vision and driving in the dark, when the pupil is enlarged.<br><br>"
                        + "Geometrical Design<br>"
                        + "Unique mechanical design of the haptics ensures<br>"
                        + "•\tExcellent stability and centration.<br>"
                        + "•\tWide angle of contact with the capsular bag.<br>"
                        + "•\t360˚ sharp square edge for lowered PCO.<br><br>"
                        + "•\tEasily and safely injected through an incision as small as 1.8 mm"
                        + "</p>";

        html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);

        return v;
    }

    private void initView(View v) {
        upload_icon = v.findViewById(R.id.upload_icon);
        backBtn = v.findViewById(R.id.backBtn);
        html_TV = v.findViewById(R.id.html_TV);
        envelope = v.findViewById(R.id.envelope);
        tech_specs_btn = v.findViewById(R.id.tech_specs_btn);
        plus_btn_1 = v.findViewById(R.id.plus_btn_1);
        plus_btn_2 = v.findViewById(R.id.plus_btn_2);
        plus_btn_3 = v.findViewById(R.id.plus_btn_3);
        plus_btn_4 = v.findViewById(R.id.plus_btn_4);
        plus_btn_5 = v.findViewById(R.id.plus_btn_5);
        brochure_btn = v.findViewById(R.id.brochure_btn);
        clinical_report_btn = v.findViewById(R.id.clinical_report_btn);
        video = v.findViewById(R.id.video);
        list_icon = v.findViewById(R.id.list_icon);
        tableImage = v.findViewById(R.id.tableImage);

    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        tech_specs_btn.setOnClickListener(this);
        plus_btn_1.setOnClickListener(this);
        plus_btn_2.setOnClickListener(this);
        plus_btn_3.setOnClickListener(this);
        plus_btn_4.setOnClickListener(this);
        plus_btn_5.setOnClickListener(this);
        brochure_btn.setOnClickListener(this);
        clinical_report_btn.setOnClickListener(this);
        video.setOnClickListener(this);
        upload_icon.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.upload_icon:
                SendByEmailDialog.newInstance(
                        Constants.VIS_TOR_BROCHURE,
                        Constants.VIS_TOR_SLIDE)
                        .show(masterActivity.getSupportFragmentManager(), "SendByEmailDialog");
                break;
            case R.id.tech_specs_btn:
                if (showSpecs) {
                    showSpecs = false;
                    tech_specs_btn.setText(masterActivity.getString(R.string.back_txt));
                    html_TV.setVisibility(View.GONE);
                    tableImage.setVisibility(View.VISIBLE);
                } else {
                    showSpecs = true;
                    tech_specs_btn.setText(masterActivity.getString(R.string.tech_specs));
                    html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
                    html_TV.setVisibility(View.VISIBLE);
                    tableImage.setVisibility(View.GONE);
                }
                break;
            case R.id.brochure_btn:
                new OpenLocalPDF(getContext(), Constants.VIS_TOR_BROCHURE).execute();
                break;
            case R.id.clinical_report_btn:
                new OpenLocalPDF(getContext(), Constants.VIS_TOR_SLIDE).execute();
                break;
            case R.id.video:
                masterActivity.showVideoFragment(Constants.VIS_TOR_VIDEO);
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
            case R.id.plus_btn_1:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_VISTOR_HOT_SPOT_1);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_2:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_VISTOR_HOT_SPOT_2);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_3:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_VISTOR_HOT_SPOT_3);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_4:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_VISTOR_HOT_SPOT_4);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_5:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_VISTOR_HOT_SPOT_5);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
        }

    }

}
