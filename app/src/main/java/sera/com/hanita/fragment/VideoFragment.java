package sera.com.hanita.fragment;


import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends Fragment implements View.OnClickListener {

    private MasterActivity masterActivity;
    private VideoView videoView;
    private MediaController mediaController;
    private int position = 0;
    private TextView closeTV;


    public static String TAG = "VideoFragment";

    public VideoFragment() {
        // Required empty public constructor
    }

    public static VideoFragment newInstance(int file) {

        VideoFragment fragment = new VideoFragment();
        Bundle args = new Bundle();
        args.putInt("file", file);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_video, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        setListeners();
        initVideo();
        return v;
    }

    private void initView(View v) {
        videoView = v.findViewById(R.id.videoView);
        closeTV = v.findViewById(R.id.closeTV);
    }

    private void setListeners() {
        closeTV.setOnClickListener(this);
    }

    private void initVideo() {
        if (mediaController == null) {
            mediaController = new MediaController(masterActivity);
            // Set the videoView that acts as the anchor for the MediaController.
            mediaController.setAnchorView(videoView);
            // Set MediaController for VideoView
            videoView.setMediaController(mediaController);
        }

        try {
            videoView.setVideoURI(Uri.parse("android.resource://" + masterActivity.getPackageName() + "/" + getArguments().getInt("file")));
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        videoView.requestFocus();
        // When the video file ready for playback.
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {

                videoView.seekTo(position);
                if (position == 0) {
                    videoView.start();
                }
                // When video Screen change size.
                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                        // Re-Set the videoView that acts as the anchor for the MediaController
                        mediaController.setAnchorView(videoView);
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.closeTV:
                masterActivity.videoFragmentOnBackPressed();
                break;
        }
    }

}
