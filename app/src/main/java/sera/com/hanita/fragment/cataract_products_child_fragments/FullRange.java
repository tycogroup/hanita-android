package sera.com.hanita.fragment.cataract_products_child_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.dialog.dialog_fragment.CommonPopUpDialog;
import sera.com.hanita.dialog.dialog_fragment.SendByEmailDialog;
import sera.com.hanita.fragment.NotificationFragment;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;

/**
 * A simple {@link Fragment} subclass.
 */
public class FullRange extends Fragment implements View.OnClickListener {


    private TextView backBtn;
    private TextView html_TV;
    private TextView tech_specs_btn;
    private MasterActivity masterActivity;
    private CommonPopUpDialog commonPopUpDialog;
    private ImageView plus_btn_1;
    private ImageView plus_btn_2;
    private ImageView plus_btn_3;
    private ImageView plus_btn_4;
    private ImageView plus_btn_5;
    private ImageView plus_btn_6;
    private ImageView upload_icon;
    private ImageView envelope;
    private ImageView list_icon;
    private ImageView tableImage;
    private boolean showSpecs = true;
    private String htmlString;
    private Button brochure_btn;
    private Button professor_btn;

    public FullRange() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_full_range, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();

        htmlString =
                "<p><font color='black'> FullRange optic lenses </font>are proven, highly reliable and safe intraocular lenses designed to provide a solution for presbyopia while reducing known complications associated with most diffractive lenses. These easy-to-implant, multifocal intraocular lenses provide your patients with excellent depth of focus for the near, intermediate and far visual ranges – and with minimal risk of complications and the need for future touch-ups or laser corrections.\n" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "\n" + "</p>";

        html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
        return v;
    }

    private void initView(View v) {
        upload_icon = v.findViewById(R.id.upload_icon);
        envelope = v.findViewById(R.id.envelope);
        backBtn = v.findViewById(R.id.backBtn);
        html_TV = v.findViewById(R.id.html_TV);
        tech_specs_btn = v.findViewById(R.id.tech_specs_btn);
        plus_btn_1 = v.findViewById(R.id.plus_btn_1);
        plus_btn_2 = v.findViewById(R.id.plus_btn_2);
        plus_btn_3 = v.findViewById(R.id.plus_btn_3);
        plus_btn_4 = v.findViewById(R.id.plus_btn_4);
        plus_btn_5 = v.findViewById(R.id.plus_btn_5);
        plus_btn_6 = v.findViewById(R.id.plus_btn_6);
        brochure_btn = v.findViewById(R.id.brochure_btn);
        professor_btn = v.findViewById(R.id.professor_btn);
        list_icon = v.findViewById(R.id.list_icon);
        tableImage = v.findViewById(R.id.tableImage);
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        brochure_btn.setOnClickListener(this);
        professor_btn.setOnClickListener(this);
        tech_specs_btn.setOnClickListener(this);
        plus_btn_1.setOnClickListener(this);
        plus_btn_2.setOnClickListener(this);
        plus_btn_3.setOnClickListener(this);
        plus_btn_4.setOnClickListener(this);
        plus_btn_5.setOnClickListener(this);
        plus_btn_6.setOnClickListener(this);
        upload_icon.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.upload_icon:
                SendByEmailDialog.newInstance(
                        Constants.FULL_RANGE)
                        .show(masterActivity.getSupportFragmentManager(), "SendByEmailDialog");
                break;
            case R.id.brochure_btn:
                new OpenLocalPDF(getContext(), Constants.FULL_RANGE).execute();
                break;
            case R.id.professor_btn:
                // add
                break;
            case R.id.tech_specs_btn:
                if (showSpecs) {
                    showSpecs = false;
                    tech_specs_btn.setText(masterActivity.getString(R.string.back_txt));
                    html_TV.setVisibility(View.GONE);
                    tableImage.setVisibility(View.VISIBLE);
                } else {
                    showSpecs = true;
                    tech_specs_btn.setText(masterActivity.getString(R.string.tech_specs));
                    html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
                    html_TV.setVisibility(View.VISIBLE);
                    tableImage.setVisibility(View.GONE);
                }
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
            case R.id.plus_btn_1:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_FULL_RANGE_HOT_SPOT_6);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_2:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_FULL_RANGE_HOT_SPOT_5);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_3:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_FULL_RANGE_HOT_SPOT_4);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_4:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_FULL_RANGE_HOT_SPOT_3);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_5:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_FULL_RANGE_HOT_SPOT_2);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_6:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_FULL_RANGE_HOT_SPOT_1);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
        }


    }

}
