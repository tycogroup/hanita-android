package sera.com.hanita.fragment;


import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Util;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragmentNew extends Fragment implements Player.EventListener {

    public static String TAG = "VideoFragmentNew";

    private SimpleExoPlayer player;
    private PlayerView playerView;
    private Boolean shouldAutoPlay = true;
    private Boolean playWhenReady = false;
    private DefaultTrackSelector trackSelector = null;
    private TrackGroupArray lastSeenTrackGroupArray = null;
    private DataSource.Factory mediaDataSourceFactory;
    private BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
    private int currentWindow = 0;
    private Long playbackPosition = 0L;
    private ImageView ivHideControllerButton;
    private ImageView ivSettings;
    private MediaSource mediaSource;
    private MasterActivity masterActivity;
    private TextView closeTV;

    public VideoFragmentNew() {
        // Required empty public constructor
    }

    public static VideoFragmentNew newInstance(String file) {

        VideoFragmentNew fragment = new VideoFragmentNew();
        Bundle args = new Bundle();
        args.putString("file", file);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_video_fragment_new, container, false);
        masterActivity = (MasterActivity) getActivity();
        playerView = v.findViewById(R.id.player_view);
        closeTV = v.findViewById(R.id.closeTV);
        ivHideControllerButton = v.findViewById(R.id.exo_controller);
        ivSettings = v.findViewById(R.id.settings);
        shouldAutoPlay = true;
        mediaDataSourceFactory = new DefaultDataSourceFactory(masterActivity, Util.getUserAgent(masterActivity, "mediaPlayerSample"),
                (TransferListener<? super DataSource>) bandwidthMeter);
        closeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                masterActivity.onBackPressed();
            }
        });
        return v;
    }

    private void initializePlayer() {
        playerView.requestFocus();
        trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
        lastSeenTrackGroupArray = null;
        player = ExoPlayerFactory.newSimpleInstance(masterActivity, trackSelector);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
        playerView.setPlayer(player);

        mediaSource = new ExtractorMediaSource.Factory(mediaDataSourceFactory)
                .createMediaSource(Uri.parse("asset:///" + getArguments().getString("file")));

        boolean haveStartPosition = currentWindow != C.INDEX_UNSET;
        if (haveStartPosition) {
            player.seekTo(currentWindow, playbackPosition);
        }
        player.prepare(mediaSource, !haveStartPosition, false);
        //updateButtonVisibilities();
        ivHideControllerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playerView.hideController();
            }
        });
        player.addListener(this);
        player.setPlayWhenReady(true);
    }

    private void releasePlayer() {
        if (player != null) {
            updateStartPosition();
            shouldAutoPlay = playWhenReady;
            player.release();
            player = null;
            trackSelector = null;
        }

    }

    private void updateStartPosition() {
        playbackPosition = player.getCurrentPosition();
        currentWindow = player.getCurrentWindowIndex();
        playWhenReady = player.getPlayWhenReady();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            initializePlayer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if ((Util.SDK_INT <= 23 || player == null)) {
            initializePlayer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }
}
