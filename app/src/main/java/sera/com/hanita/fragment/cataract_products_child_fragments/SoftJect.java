package sera.com.hanita.fragment.cataract_products_child_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.dialog.dialog_fragment.SendByEmailDialog;
import sera.com.hanita.fragment.NotificationFragment;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;

/**
 * A simple {@link Fragment} subclass.
 */
public class SoftJect extends Fragment implements View.OnClickListener {

    public static final int txt_2_4 = 1;
    public static final int txt_1_8 = 2;
    private ImageView envelope;
    private TextView main_txt;
    private TextView backBtn;
    private TextView top_tv;
    private MasterActivity masterActivity;
    private Button brochure_btn;
    private Button instructions;
    private ImageView upload_icon;
    private ImageView list_icon;

    public SoftJect() {
        // Required empty public constructor
    }

    public static SoftJect newInstance(int witchTxt) {
        SoftJect fragment = new SoftJect();
        Bundle args = new Bundle();
        args.putInt("txt", witchTxt);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_soft_ject, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();
        String htmlString =
                "<p><font color='#00467e'>Designation</font><br><br>"
                        + "Intra ocular lens Injector<br><br><br><br>"
                        + "<font color='#00467e'>Description:</font><br><br>"
                        + "•\ta sub assembly: injector with a body, a pusher, a metal spring and a plunger.<br>"
                        + "•\ta cartridge Medicel Ref. LC 604251<br>"
                        + "•\tAn accessory: Reamer<br><br>"
                        + "<font color='#00467e'>Raw material:</font><br>"
                        + "(MABS) Methyl Methacrylate Acrylonitrile Butadiene Styrene,<br>"
                        + "(POM) Polyoxymethylene,<br>"
                        + "(SEBS) Styrene Ethylene Butadiene Styrene,<br>"
                        + "(PP) Polypropylene,<br>"
                        + "(inox steel AISI 302).<br>"
                        + "Latex free<br>"
                        + "</p>";


        main_txt.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
        if (Objects.requireNonNull(getArguments()).getInt("txt") == txt_2_4) {
            top_tv.setText(getString(R.string.soft_ject_2_4));
        } else {
            top_tv.setText(getString(R.string.soft_ject_1_8));
        }
        return v;
    }

    private void initView(View v) {
        upload_icon = v.findViewById(R.id.upload_icon);
        main_txt = v.findViewById(R.id.main_txt);
        envelope = v.findViewById(R.id.envelope);
        backBtn = v.findViewById(R.id.backBtn);
        top_tv = v.findViewById(R.id.top_tv);
        brochure_btn = v.findViewById(R.id.brochure_btn);
        instructions = v.findViewById(R.id.instructions);
        list_icon = v.findViewById(R.id.list_icon);
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        brochure_btn.setOnClickListener(this);
        instructions.setOnClickListener(this);
        upload_icon.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.upload_icon:
                if (Objects.requireNonNull(getArguments()).getInt("txt") == txt_2_4) {
                    SendByEmailDialog.newInstance(
                            Constants.SOFT_JECT_INSTRUCTIONS_2_4,
                            Constants.SOFT_JECT_BROCHURE_2_4)
                            .show(masterActivity.getSupportFragmentManager(), "SendByEmailDialog");
                } else {
                    SendByEmailDialog.newInstance(
                            Constants.SOFT_JECT_INSTRUCTIONS_1_8,
                            Constants.SOFT_JECT_BROCHURE_1_8)
                            .show(masterActivity.getSupportFragmentManager(), "SendByEmailDialog");
                }

                break;
            case R.id.instructions:
                if (Objects.requireNonNull(getArguments()).getInt("txt") == txt_2_4) {
                    new OpenLocalPDF(getContext(), Constants.SOFT_JECT_INSTRUCTIONS_2_4).execute();
                } else {
                    new OpenLocalPDF(getContext(), Constants.SOFT_JECT_INSTRUCTIONS_1_8).execute();
                }
                break;
            case R.id.brochure_btn:
                if (Objects.requireNonNull(getArguments()).getInt("txt") == txt_2_4) {
                    new OpenLocalPDF(getContext(), Constants.SOFT_JECT_BROCHURE_2_4).execute();
                } else {
                    new OpenLocalPDF(getContext(), Constants.SOFT_JECT_BROCHURE_1_8).execute();
                }
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
        }
    }
}
