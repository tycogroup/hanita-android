package sera.com.hanita.fragment.cataract_products_child_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.dialog.dialog_fragment.CommonPopUpDialog;
import sera.com.hanita.dialog.dialog_fragment.SendByEmailDialog;
import sera.com.hanita.fragment.NotificationFragment;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;

/**
 * A simple {@link Fragment} subclass.
 */
public class SeeLens extends Fragment implements View.OnClickListener {


    private TextView backBtn;
    private MasterActivity masterActivity;
    private CommonPopUpDialog commonPopUpDialog;
    private ImageView plus_btn_1;
    private ImageView plus_btn_2;
    private ImageView plus_btn_3;
    private ImageView upload_icon;
    private ImageView envelope;
    private ImageView list_icon;
    private TextView html_TV;
    private TextView tech_specs_btn;
    private String htmlString;
    private boolean showSpecs = true;
    private Button brochure_btn;
    private Button clinical_report_btn;
    private Button clinical_exp_2y;
    private ImageView tableImage;

    public SeeLens() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_see_lens, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();


        htmlString =
                " <p><font color='#a9d175'>Capsular Bag</font><br>"
                        + "SeeLens design combines the classic C loop and plate-haptic in order to fit in the bag .<br>"
                        + "Square edge stepped barrier design<br>"
                        + "Reduces PCO.<br>"
                        + "Helps prevent cell migration onto the posterior capsule.<br><br>"
                        + "<font color='#a9d175'>High Refractive Index</font><br>"
                        + "SeeLens high refractive index (1.462) optimizes the profile of the lens enabling insertion through a 2.4 mm incision.<br>"
                        + "Slow Gentle Release<br>"
                        + "SeeLens material has excellent memory properties that ensure a perfect foldability and perfectly slow and gentle unfolding release.<br><br>"
                        + "The result is an accurate simulation of the visual performance of the BunnyLens AF in the Post- operative eye.<br>"
                        + "[1] Field Guide to Visual and Ophthalmic Optics; Jim Schwiegerling; Nov. 2004.<br>" + "</p>";

        html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);

        return v;
    }

    private void initView(View v) {
        upload_icon = v.findViewById(R.id.upload_icon);

        backBtn = v.findViewById(R.id.backBtn);
        html_TV = v.findViewById(R.id.html_TV);
        tech_specs_btn = v.findViewById(R.id.tech_specs_btn);
        plus_btn_1 = v.findViewById(R.id.plus_btn_1);
        plus_btn_2 = v.findViewById(R.id.plus_btn_2);
        plus_btn_3 = v.findViewById(R.id.plus_btn_3);
        brochure_btn = v.findViewById(R.id.brochure_btn);
        clinical_report_btn = v.findViewById(R.id.clinical_report_btn);
        clinical_exp_2y = v.findViewById(R.id.clinical_exp_2y);
        envelope = v.findViewById(R.id.envelope);
        list_icon = v.findViewById(R.id.list_icon);
        tableImage = v.findViewById(R.id.tableImage);

    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        tech_specs_btn.setOnClickListener(this);
        plus_btn_1.setOnClickListener(this);
        plus_btn_2.setOnClickListener(this);
        plus_btn_3.setOnClickListener(this);
        brochure_btn.setOnClickListener(this);
        clinical_report_btn.setOnClickListener(this);
        clinical_exp_2y.setOnClickListener(this);
        upload_icon.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.upload_icon:
                SendByEmailDialog.newInstance(
                        Constants.SEE_LENS_BROCHURE,
                        Constants.SEE_LENS_CLINICAL,
                        Constants.SEE_LENS_CLINICAL_2_Y)
                        .show(masterActivity.getSupportFragmentManager(), "SendByEmailDialog");
                break;
            case R.id.brochure_btn:
                new OpenLocalPDF(getContext(), Constants.SEE_LENS_BROCHURE).execute();
                break;
            case R.id.clinical_report_btn:
                new OpenLocalPDF(getContext(), Constants.SEE_LENS_CLINICAL).execute();
                break;
            case R.id.clinical_exp_2y:
                new OpenLocalPDF(getContext(), Constants.SEE_LENS_CLINICAL_2_Y).execute();
                break;
            case R.id.tech_specs_btn:
                if (showSpecs) {
                    showSpecs = false;
                    tech_specs_btn.setText(masterActivity.getString(R.string.back_txt));
                    html_TV.setVisibility(View.GONE);
                    tableImage.setVisibility(View.VISIBLE);
                } else {
                    showSpecs = true;
                    tech_specs_btn.setText(masterActivity.getString(R.string.tech_specs));
                    html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
                    html_TV.setVisibility(View.VISIBLE);
                    tableImage.setVisibility(View.GONE);
                }
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
            case R.id.plus_btn_1:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_SEE_LENS_HOT_SPOT_2);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_2:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_SEE_LENS_HOT_SPOT_3);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_3:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_SEE_LENS_HOT_SPOT_1);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
        }


    }

}
