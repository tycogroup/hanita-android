package sera.com.hanita.fragment;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.adapter.SurgeriePagerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class SurgerieFragment extends Fragment implements View.OnClickListener {

    private ViewPager viewPager;
    private MasterActivity masterActivity;
    private TextView backBtn;
    private ImageView envelope;
    private ImageView list_icon;

    public SurgerieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_surgerie, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();
        setUpPager();
        return v;
    }

    private void setUpPager() {
        List<Drawable> list = new ArrayList<>();
        list.add(getResources().getDrawable(R.drawable.slide_1_surg)); // VisTor
        list.add(getResources().getDrawable(R.drawable.slide_2)); // SeelensMF
        list.add(getResources().getDrawable(R.drawable.slide_3)); // SeeLEnsHP (another doctor)
        list.add(getResources().getDrawable(R.drawable.slide_4)); // SeeLEnsHP
        list.add(getResources().getDrawable(R.drawable.slide_5)); // SeeLEnsAF
        list.add(getResources().getDrawable(R.drawable.slide_6)); // SeeLens
        list.add(getResources().getDrawable(R.drawable.slide_7)); // AssiAnchor
        viewPager.setAdapter(new SurgeriePagerAdapter(masterActivity, list));
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }

    private void initView(View v) {
        viewPager = v.findViewById(R.id.viewPager);
        backBtn = v.findViewById(R.id.backBtn);
        envelope = v.findViewById(R.id.envelope);
        list_icon = v.findViewById(R.id.list_icon);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.backBtn:
                masterActivity.onBackPressed();
                break;
        }
    }
}
