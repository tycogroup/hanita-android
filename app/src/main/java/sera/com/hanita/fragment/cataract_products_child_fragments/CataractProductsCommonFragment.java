package sera.com.hanita.fragment.cataract_products_child_fragments;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.adapter.CataractProductsCommonAdapter;
import sera.com.hanita.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class CataractProductsCommonFragment extends Fragment implements View.OnClickListener {

    private RecyclerView recyclerView;
    private CataractProductsCommonAdapter cataractProductsCommonAdapter;
    private TextView backBtn;
    private MasterActivity masterActivity;
    private ImageView list_icon;

    public CataractProductsCommonFragment() {
        // Required empty public constructor
    }

    public static CataractProductsCommonFragment newInstance(int position) {
        CataractProductsCommonFragment f = new CataractProductsCommonFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_cataract_products_common, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();
        generateAdapter();
        recyclerView.setAdapter(cataractProductsCommonAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
        recyclerView.hasFixedSize();

        return v;
    }

    private void initView(View v) {
        recyclerView = v.findViewById(R.id.recyclerView);
        backBtn = v.findViewById(R.id.backBtn);
        list_icon = v.findViewById(R.id.list_icon);
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }

    private void generateAdapter() {
        cataractProductsCommonAdapter = new CataractProductsCommonAdapter(
                getContext(),
                getList(Objects.requireNonNull(getArguments()).getInt("position")),
                getNames(getArguments().getInt("position")),
                getDesc(getArguments().getInt("position")),
                this);
    }

    private List<String> getNames(int position) {
        List<String> list = new ArrayList<>();
        switch (position) {
            case 0:
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.hp_easy_title));
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.see_lens_hp));
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.bunny_lens_hp));
                break;
            case 1:
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.b_lens));
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.see_lens));
                break;
            case 2:
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.see_lens_af));
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.bunny_lens_af));
                break;
            case 3:
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.fullrange));
                break;
            case 4:
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.perfecTor));
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.vis_tor));
                break;
            case 5:
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.visco_1_4));
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.visco_1_8));
                break;
            case 6:
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.soft_ject_1_8));
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.soft_ject_2_4));
                break;
            case 7:
                list.add(Objects.requireNonNull(getContext()).getResources().getString(R.string.assi_anchor_txt));
                break;
        }
        return list;
    }

    private List<String> getDesc(int position) {
        List<String> list = new ArrayList<>();
        switch (position) {
            case 0:
                list.add("Preloaded Hydrophobic IOL System");
                list.add("Phydrophobic Aspheric IOL");
                list.add("Hydrophobic Aspheric IOL");
                break;
            case 1:
                list.add("Optimum postoperative visual acuity");
                list.add("New - Square edge design");
                break;
            case 2:
                list.add("Providing aberration free vision");
                list.add("Providing aberration free vision");
                break;
            case 3:
                list.add("The Premium Multifocal solution");
                break;
            case 4:
                list.add("Visitor foldable IOL");
                list.add("The premium toric solution");
                break;
            case 5:
                list.add("Ophthalmic Viscosurgical Device (OVD)");
                list.add("Ophthalmic Viscosurgical Device (OVD)");
                break;
            case 6:
                list.add("Single use delivery system");
                list.add("Single use delivery system");
                break;
            case 7:
                list.add("Novel capsular anchor device");
                break;
            case 8:
                list.add("Preloaded Hydrophobic IOL System");
                break;
        }
        return list;
    }

    private List<Drawable> getList(int position) {
        List<Drawable> targetList = new ArrayList<>();
        switch (position) {
            case 0:
                targetList.add(getResources().getDrawable(R.drawable.vector_smart_object_easy_hp_2));
                targetList.add(getResources().getDrawable(R.drawable.see_hp_menu));
                targetList.add(getResources().getDrawable(R.drawable.bunny_hp_menu));
                break;
            case 1:
                targetList.add(getResources().getDrawable(R.drawable.blens_menu));
                targetList.add(getResources().getDrawable(R.drawable.see_lens_menu));
                break;
            case 2:
                targetList.add(getResources().getDrawable(R.drawable.see_af_menu));
                targetList.add(getResources().getDrawable(R.drawable.bunny_af_menu));
                break;
            case 3:
                targetList.add(getResources().getDrawable(R.drawable.see_af_menu));
                break;
            case 4:
                targetList.add(getResources().getDrawable(R.drawable.vistor_menu));
                targetList.add(getResources().getDrawable(R.drawable.vistor_menu));
                break;
            case 5:
                targetList.add(getResources().getDrawable(R.drawable.visco_menu));
                targetList.add(getResources().getDrawable(R.drawable.visco_menu));
                break;
            case 6:
                targetList.add(getResources().getDrawable(R.drawable.soft_menu));
                targetList.add(getResources().getDrawable(R.drawable.soft_menu));
                break;
            case 7:
                targetList.add(getResources().getDrawable(R.drawable.assianchor_menu));
                break;
            case 8:
                targetList.add(getResources().getDrawable(R.drawable.assianchor_menu));
                break;
        }
        return targetList;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
        }
    }

    public void openNextChildFragment(int secondPosition) {
        switch (Objects.requireNonNull(getArguments()).getInt("position")) {
            case Constants.HYDROPHONIC:
                switch (secondPosition) {
                    case 0:
                        masterActivity.showChildFragment(new HpEasy());
                        break;
                    case 1:
                        masterActivity.showChildFragment(new SeeLensHP());
                        break;
                    case 2:
                        masterActivity.showChildFragment(new BunnyLensHP());
                        break;

                }
                break;
            case Constants.SPHERIC_IOLS:
                switch (secondPosition) {
                    case 0:
                        masterActivity.showChildFragment(new BLens());
                        break;
                    case 1:
                        masterActivity.showChildFragment(new SeeLens());
                        break;
                }
                break;
            case Constants.ASPHERIC_IOLS:
                switch (secondPosition) {
                    case 0:
                        masterActivity.showChildFragment(new SeeLensAF());
                        break;
                    case 1:
                        masterActivity.showChildFragment(new BunnyLensAF());
                        break;

                }
                break;
            case Constants.FULL_RANGE_IOLS:
                if (secondPosition == 0) {
                    masterActivity.showChildFragment(new FullRange());
                }
                break;
            case Constants.TORIC_IOLS:
                switch (secondPosition) {
                    case 0:
                        masterActivity.showChildFragment(new PerfecTor());
                        break;
                    case 1:
                        masterActivity.showChildFragment(new VisTorFragment());
                        break;
                }
                break;
            case Constants.OVD:
                switch (secondPosition) {
                    case 0:
                        masterActivity.showChildFragment(HanitaVisco1_4.newInstance(HanitaVisco1_4.txt_1_4));
                        break;
                    case 1:
                        masterActivity.showChildFragment(HanitaVisco1_4.newInstance(HanitaVisco1_4.txt_1_8));
                        break;
                }
                break;
            case Constants.ACCESSORIES:
                switch (secondPosition) {
                    case 0:
                        masterActivity.showChildFragment(SoftJect.newInstance(SoftJect.txt_1_8));
                        break;
                    case 1:
                        masterActivity.showChildFragment(SoftJect.newInstance(SoftJect.txt_2_4));
                        break;
                }
                break;
            case Constants.MISCELLANEOUS:
                masterActivity.showChildFragment(new AssoAnchorFragment());
                break;
            case Constants.HP_EASY:
                masterActivity.showChildFragment(new HpEasy());
                break;
        }

    }

}
