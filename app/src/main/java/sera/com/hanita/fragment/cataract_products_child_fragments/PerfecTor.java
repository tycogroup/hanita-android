package sera.com.hanita.fragment.cataract_products_child_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.fragment.NotificationFragment;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;

/**
 * A simple {@link Fragment} subclass.
 */
public class PerfecTor extends Fragment implements View.OnClickListener {

    private TextView backBtn;
    private TextView main_txt;
    private MasterActivity masterActivity;
    private boolean showSpecs = true;
    private String htmlString;
    private ImageView envelope;
    private ImageView list_icon;
    private TextView tech_specs_btn;
    private Button brochure_btn;
    private Button professor_btn;
    private ImageView tableImage;

    public PerfecTor() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_perfector, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();
        htmlString =
                "A3 PerfeccTor<br>"
                        + "The PerfecTor is an aspheric IOL, designed and manufactured to ensure that surgeons Always Address Astigmatism and to restore and improve" +
                        " an astigmatism for the impaired patient." +
                        "The PrefecTor treats patients ranging 0.5D up to 1.75D of corneal astigmatism, tha majority of patients (59.2%) are included in this group.<br>"
                        + "</p>";

        main_txt.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
        return v;
    }

    private void initView(View v) {
        tech_specs_btn = v.findViewById(R.id.tech_specs_btn);
        backBtn = v.findViewById(R.id.backBtn);
        envelope = v.findViewById(R.id.envelope);
        list_icon = v.findViewById(R.id.list_icon);
        main_txt = v.findViewById(R.id.main_txt);
        brochure_btn = v.findViewById(R.id.brochure_btn);
        professor_btn = v.findViewById(R.id.professor_btn);
        tableImage = v.findViewById(R.id.tableImage);

    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
        tech_specs_btn.setOnClickListener(this);
        brochure_btn.setOnClickListener(this);
        professor_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.professor_btn:
                new OpenLocalPDF(getContext(), Constants.PERFECTOR_PROFESSOR).execute();
                break;
            case R.id.brochure_btn:
                new OpenLocalPDF(getContext(), Constants.PERFECTOR_BROCURE).execute();
                break;
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
            case R.id.tech_specs_btn:
                if (showSpecs) {
                    showSpecs = false;
                    tech_specs_btn.setText(masterActivity.getString(R.string.back_txt));
                    main_txt.setVisibility(View.GONE);
                    tableImage.setVisibility(View.VISIBLE);

                } else {
                    showSpecs = true;
                    tech_specs_btn.setText(masterActivity.getString(R.string.tech_specs));
                    main_txt.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
                    main_txt.setVisibility(View.VISIBLE);
                    tableImage.setVisibility(View.GONE);
                }
                break;
        }
    }

}
