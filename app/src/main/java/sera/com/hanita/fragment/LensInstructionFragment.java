package sera.com.hanita.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.adapter.LensesPagerAdapter;
import sera.com.hanita.utils.CallBack;
import sera.com.hanita.utils.Constants;

public class LensInstructionFragment extends Fragment implements View.OnClickListener, CallBack {

    private TextView backBtn;
    private MasterActivity masterActivity;
    private ImageView envelope;
    private ImageView list_icon;
    private ViewPager lensViewPager;

    public LensInstructionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_lens_instruction, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        setListeners();
        setUpPager();
        return v;
    }


    private void setUpPager() {
        List<Drawable> list = new ArrayList<>();
        list.add(getResources().getDrawable(R.drawable.slide_1));
        list.add(getResources().getDrawable(R.drawable.slide_1_copy));
        list.add(getResources().getDrawable(R.drawable.hp_easy_see_lens_copy));
        list.add(getResources().getDrawable(R.drawable.hp_easy_bunny_lens_copy));
        lensViewPager.setAdapter(new LensesPagerAdapter(this, list));
    }

    private void setListeners() {
        backBtn.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }

    private void initView(View v) {
        backBtn = v.findViewById(R.id.backBtn);
        lensViewPager = v.findViewById(R.id.lensViewPager);
        envelope = v.findViewById(R.id.envelope);
        list_icon = v.findViewById(R.id.list_icon);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
//            case R.id.leftIV:
//                masterActivity.showVideoFragment(Constants.SOFTJECT_2_4_BIG);
//                break;
//            case R.id.rightIV:
//                masterActivity.showVideoFragment(Constants.ACCUJET_PRELOADED);
//                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
        }
    }

    @Override
    public void onItemClicked(Integer position) {
        switch (position) {
            case Constants.SOFTJECT:
                masterActivity.showVideoFragment(Constants.SOFTJECT_2_4_BIG);
                break;
            case Constants.ACCUJET:
                masterActivity.showVideoFragment(Constants.ACCUJET_PRELOADED);
                break;
            case Constants.SEE_LENS:
                masterActivity.showVideoFragment(Constants.HP_EASY_SEE_LENS);
                break;
            case Constants.BUNNY_LENS:
                masterActivity.showVideoFragment(Constants.HP_EASY_BUNNY_LENS);
                break;
        }
    }
}
