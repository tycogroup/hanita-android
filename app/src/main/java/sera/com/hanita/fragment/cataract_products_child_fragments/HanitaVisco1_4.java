package sera.com.hanita.fragment.cataract_products_child_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.dialog.dialog_fragment.CommonPopUpDialog;
import sera.com.hanita.dialog.dialog_fragment.SendByEmailDialog;
import sera.com.hanita.fragment.NotificationFragment;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;

/**
 * A simple {@link Fragment} subclass.
 */
public class HanitaVisco1_4 extends Fragment implements View.OnClickListener {

    public static final int txt_1_4 = 1;
    public static final int txt_1_8 = 2;
    private TextView backBtn;
    private TextView html_TV;
    private TextView top_tv;
    private MasterActivity masterActivity;
    private CommonPopUpDialog commonPopUpDialog;
    private Button brochure_btn;
    private ImageView upload_icon;
    private ImageView envelope;
    private ImageView list_icon;

    public HanitaVisco1_4() {
        // Required empty public constructor
    }

    public static HanitaVisco1_4 newInstance(int witchTxt) {
        HanitaVisco1_4 fragment = new HanitaVisco1_4();
        Bundle args = new Bundle();
        args.putInt("txt", witchTxt);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_hanita_visco1_4, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();
        String htmlString =
                "HanitaVisco is an Ophthalmic Viscosurgical Device ( OVD ) Substance Sodium Hyaluronate from biofermentation, eliminating the need for materials of animal origin.<br><br>"
                        + "HanitaVisco is a CE marked OVD according to MDD 93/42/EEC<br><br><br>"
                        + "HanitaVisco is one of the most elastic materials produced using this source with three characteristics viscosities in three very important shear rate regions<br><br><br>"
                        + "The zero shear rate viscosity is responsible for the maintenance of space in the anterior chamber and allows the manipulation of tissues during the operation without escaping trough the incision.<br><br>"
                        + "The surgical shear rate viscosity determines the resistance to the instruments and implant during intraocular procedures. It has to provide free movement for the instruments and an easy entry for the implant into the inflated capsular bag.<br><br>"
                        + "The injection shear rate viscosity causes the resistance when expressing HanitaVisco trough a cannula.<br><br><br>"
                        + "The viscosity and elasticity range of HanitaVisco have been selected to provide the highest possible zero share viscosity, while allowing unrestricted manipulation during surgery and still enabling the use of a thin, 27 gauge cannula for expression. These parameters have been fine tuned using small changes, based on feedback from our specialists and customers worldwide."
                        + "</p>";
        html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
        if (Objects.requireNonNull(getArguments()).getInt("txt") == txt_1_4) {
            top_tv.setText(getString(R.string.visco_1_4));
        } else {
            top_tv.setText(getString(R.string.visco_1_8));
        }
        return v;
    }

    private void initView(View v) {
        upload_icon = v.findViewById(R.id.upload_icon);
        backBtn = v.findViewById(R.id.backBtn);
        envelope = v.findViewById(R.id.envelope);
        html_TV = v.findViewById(R.id.html_TV);
        top_tv = v.findViewById(R.id.top_tv);
        brochure_btn = v.findViewById(R.id.brochure_btn);
        list_icon = v.findViewById(R.id.list_icon);
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        brochure_btn.setOnClickListener(this);
        envelope.setOnClickListener(this);
        upload_icon.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.upload_icon:
                if (Objects.requireNonNull(getArguments()).getInt("txt") == txt_1_4) {
                    SendByEmailDialog.newInstance(
                            Constants.VISCO_1_4_BROCHURE)
                            .show(masterActivity.getSupportFragmentManager(), "SendByEmailDialog");
                } else {
                    SendByEmailDialog.newInstance(
                            Constants.VISCO_1_8_BROCHURE)
                            .show(masterActivity.getSupportFragmentManager(), "SendByEmailDialog");
                }
                break;
            case R.id.brochure_btn:
                if (Objects.requireNonNull(getArguments()).getInt("txt") == txt_1_4) {
                    new OpenLocalPDF(getContext(), Constants.VISCO_1_4_BROCHURE).execute();
                } else {
                    new OpenLocalPDF(getContext(), Constants.VISCO_1_8_BROCHURE).execute();
                }
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
            case R.id.plus_btn_1:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_B_LENNS_SPOT_1);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_2:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_B_LENNS_SPOT_2);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_3:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_B_LENNS_SPOT_3);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
        }


    }

}
