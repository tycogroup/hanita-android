package sera.com.hanita.fragment.cataract_products_child_fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.dialog.dialog_fragment.CommonPopUpDialog;
import sera.com.hanita.dialog.dialog_fragment.SendByEmailDialog;
import sera.com.hanita.fragment.NotificationFragment;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;

public class BLens extends Fragment implements View.OnClickListener {

    private TextView backBtn;
    private TextView html_TV;
    private TextView tech_specs_btn;
    private MasterActivity masterActivity;
    private CommonPopUpDialog commonPopUpDialog;
    private ImageView plus_btn_1;
    private ImageView plus_btn_2;
    private ImageView plus_btn_3;
    private ImageView upload_icon;
    private ImageView envelope;
    private ImageView list_icon;
    private boolean showSpecs = true;
    private String htmlString;
    private Button brochure_btn;
    private Button qa_btn;
    private Button clinical_exp;
    private Button clinical_exp_1y;
    private ImageView tableImage;

    public BLens() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_blens, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();
        htmlString =
                "<p><font color='orange'>Improved modified C haptic shape.</font>B-Lens design optimizes the stability of<br>"
                        + "the lens in the capsular bag.<font color='orange'>Square edge design</font>B-Lens square edge <br>"
                        + "design – intended to prevent cell growth under the lens.<font color='orange'>Improved posterior</font><br>"
                        + "<font color='orange'>capsule tension</font> B-Lens 5° angulation and haptic shape, improve the <br>"
                        + "posterior capsular tension. <font color='orange'>High Refractive index</font> B-Lens High Refractive <br>"
                        + "Index (1,462), optimizes the profile of the lens thus enabling insertion <br>"
                        + "through a 2- 3.2 mm incesion. <font color='orange'>Insertion</font> B-Lens can be implanted with <br>"
                        + "either Forceps or Injector. <font color='orange'>Superior foldability with a gentle and slow</font> <br>"
                        + "<font color='orange'>unfolding release</font> B-Lens material has excellent memory properties that <br>"
                        + "ensure a perfect foldability and perfectly slow and gentle unfolding release. <br>"
                        + "<font color='orange'>UV Blocker</font> B-Lens material has a built in UV blocker that effectively filters <br>"
                        + "damaging UV radiation. <br><br>"
                        + "<font color='orange'>YAG laser compatible</font> B-Lens is YAG laser compatible."
                        + "</p>";

        html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
        return v;
    }

    private void initView(View v) {
        upload_icon = v.findViewById(R.id.upload_icon);
        envelope = v.findViewById(R.id.envelope);
        backBtn = v.findViewById(R.id.backBtn);
        html_TV = v.findViewById(R.id.html_TV);
        tech_specs_btn = v.findViewById(R.id.tech_specs_btn);
        brochure_btn = v.findViewById(R.id.brochure_btn);
        qa_btn = v.findViewById(R.id.qa_btn);
        clinical_exp = v.findViewById(R.id.clinical_exp);
        clinical_exp_1y = v.findViewById(R.id.clinical_exp_1y);
        plus_btn_1 = v.findViewById(R.id.plus_btn_1);
        plus_btn_2 = v.findViewById(R.id.plus_btn_2);
        plus_btn_3 = v.findViewById(R.id.plus_btn_3);
        list_icon = v.findViewById(R.id.list_icon);
        tableImage = v.findViewById(R.id.tableImage);
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        tech_specs_btn.setOnClickListener(this);
        plus_btn_1.setOnClickListener(this);
        plus_btn_2.setOnClickListener(this);
        plus_btn_3.setOnClickListener(this);
        brochure_btn.setOnClickListener(this);
        qa_btn.setOnClickListener(this);
        clinical_exp.setOnClickListener(this);
        envelope.setOnClickListener(this);
        clinical_exp_1y.setOnClickListener(this);
        list_icon.setOnClickListener(this);
        upload_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.upload_icon:
                SendByEmailDialog.newInstance(
                        Constants.PRODUCT_BROCHURE_BLENS,
                        Constants.QA_BLENS,
                        Constants.CLONICAL_EXP,
                        Constants.CLONICAL_EXP_1Y)
                        .show(masterActivity.getSupportFragmentManager(), "SendByEmailDialog");
                break;
            case R.id.brochure_btn:
                new OpenLocalPDF(getContext(), Constants.PRODUCT_BROCHURE_BLENS).execute();
                break;
            case R.id.qa_btn:
                new OpenLocalPDF(getContext(), Constants.QA_BLENS).execute();
                break;
            case R.id.clinical_exp:
                new OpenLocalPDF(getContext(), Constants.CLONICAL_EXP).execute();
                break;
            case R.id.clinical_exp_1y:
                new OpenLocalPDF(getContext(), Constants.CLONICAL_EXP_1Y).execute();
                break;
            case R.id.tech_specs_btn:
                if (showSpecs) {
                    showSpecs = false;
                    tech_specs_btn.setText(masterActivity.getString(R.string.back_txt));
                    html_TV.setVisibility(View.GONE);
                    tableImage.setVisibility(View.VISIBLE);
                } else {
                    showSpecs = true;
                    tech_specs_btn.setText(masterActivity.getString(R.string.tech_specs));
                    html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
                    html_TV.setVisibility(View.VISIBLE);
                    tableImage.setVisibility(View.GONE);
                }
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
            case R.id.plus_btn_1:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_B_LENNS_SPOT_1);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_2:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_B_LENNS_SPOT_2);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_3:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_B_LENNS_SPOT_3);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
        }


    }


}
