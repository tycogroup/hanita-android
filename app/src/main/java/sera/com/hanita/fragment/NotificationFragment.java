package sera.com.hanita.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.adapter.NotificationAdapter;
import sera.com.hanita.api.NetworkManager;
import sera.com.hanita.api.interfaces.RequestResultListener;
import sera.com.hanita.object.Notification;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment implements View.OnClickListener {

    private TextView backBtn;
    private MasterActivity masterActivity;
    private RecyclerView recyclerView;
    private NotificationAdapter notificationAdapter;
    private ImageView list_icon;

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_notification, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();

        notificationAdapter = new NotificationAdapter(masterActivity, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(masterActivity));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(notificationAdapter);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        callServer();
//        new PublicationTask(masterActivity, this).execute();
    }

    private void initView(View v) {
        backBtn = v.findViewById(R.id.backBtn);
        recyclerView = v.findViewById(R.id.recyclerView);
        list_icon = v.findViewById(R.id.list_icon);
        v.findViewById(R.id.envelope).setVisibility(View.GONE);
        v.findViewById(R.id.right_line).setVisibility(View.GONE);
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
            default:
                masterActivity.showWebFragment(notificationAdapter.getItem((Integer) v.getTag()),null);
                break;
        }
    }

    private void callServer() {
        new NetworkManager(masterActivity).getNotifications(new RequestResultListener<Notification>() {
            @Override
            public void onSuccess(List<Notification> result) {
                updateAdapter(result);
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(masterActivity, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void updateAdapter(List<Notification> list) {
        notificationAdapter.addItems(list);
    }
}
