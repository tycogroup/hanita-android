package sera.com.hanita.fragment.cataract_products_child_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.dialog.dialog_fragment.CommonPopUpDialog;
import sera.com.hanita.dialog.dialog_fragment.SendByEmailDialog;
import sera.com.hanita.fragment.NotificationFragment;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;

/**
 * A simple {@link Fragment} subclass.
 */
public class BunnyLensAF extends Fragment implements View.OnClickListener {

    private TextView backBtn;
    private TextView html_TV;
    private TextView tech_specs_btn;
    private MasterActivity masterActivity;
    private CommonPopUpDialog commonPopUpDialog;
    private ImageView plus_btn_1;
    private ImageView plus_btn_2;
    private ImageView plus_btn_3;
    private ImageView upload_icon;
    private ImageView envelope;
    private ImageView list_icon;
    private boolean showSpecs = true;
    private String htmlString;
    private Button brochure_btn;
    private Button clinical_report_btn;
    private Button bunny_lens_AF_btn;
    private Button accuject_Pro_btn;
    private ImageView tableImage;

    public BunnyLensAF() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_bunny_lens_af, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();

        htmlString =
//                "<p> The aspheric<font color='blue'> BunnyLens AF </font>was designed using the most advanced tools, by a professional R&amp;D team of optical and mechanical engineers.<br>"
//                        + "The optical profile  was calculated using ZEMAX ™ software – a simulating tool for the optical design optimization.<br>"
//                        + "Calculations were aimed in order to minimize all aberrations, including the spherical aberration of the cornea, and to optimize the MTF (Modulated Transfer Function) of the IOL.<br><br>"
//                        + "<font color='blue'>Eye Model</font><br>"
//                        + "The Optical design of BunnyLens AF was performed using the advanced Arizona Eye model [1].<br>"
//                        + "The parameters and dimensions of the eye model are consistent with average human data. The model was designed to match clinical levels of aberrations, both on and off axis.<br>"
//                        + "The retina curvature is designed to split the tangential and sagittal foci off-axis.<br>"
//                        + " The result is an accurate simulation of the visual performance of the BunnyLens AF in the Post- operative eye.<br>"
//                        + "[1] Field Guide to Visual and Ophthalmic Optics; Jim Schwiegerling; Nov. 2004." + "</p>";
                "<p> The aspheric <font color='#74c4df'> BunnyLens AF </font>was designed using the most advanced tools,"
                        + "by a professional R&D team of optical and mechanical engineers. The"
                        + "optical profile was calculated using ZEMAX TM software –a simulating tool"
                        + "for the optical design optimization. Calculations were aimed in order to"
                        + "minimize all aberrations, including the spherical aberration of the cornea,"
                        + "and to optimize the MTF (Modulated Transfer Function) of the IOL."
                        + "<font color='#74c4df'> Eye Model </font> The Optical design of BunnyLens AF was performed using the"
                        + "advanced Arizona Eye model [1].The parameters and dimensions of the"
                        + "eye model are consistent with average human data. The model was"
                        + "designed to match clinical levels of aberrations, both on and off axis. The"
                        + "retina curvature is designed to split the tangential and sagittal foci off-axis."
                        + "The result is an accurate simulation of the visual performance of the"
                        + "BunnyLens AF in the Post- operative eye. [1] Field Guide to Visual and"
                        + "Ophthalmic Optics; Jim Schwiegerling; Nov. 2004. <font color='#74c4df'> Geometrical Design </font> "
                        + "BunnyLens AF ensures excellent stability and centration due to four- point-"
                        + "fixated mechanical design of the haptics. 360° double square edge in order"
                        + "to minimize PCO Excellent memory – slow gentle release, superior"
                        + "foldability <font color='#74c4df'> Material </font> The BunnyLens AF is made from hydrophilic acrylic"
                        + "HEMA/ EOEMA copolymer material with a UV and Violet Light Filter,"
                        + "having a proven excellent reputation and many years of clinical experience"
                        + "</p>";

        html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
        return v;
    }

    private void initView(View v) {
        upload_icon = v.findViewById(R.id.upload_icon);
        backBtn = v.findViewById(R.id.backBtn);
        html_TV = v.findViewById(R.id.html_TV);
        envelope = v.findViewById(R.id.envelope);
        tech_specs_btn = v.findViewById(R.id.tech_specs_btn);
        plus_btn_1 = v.findViewById(R.id.plus_btn_1);
        plus_btn_2 = v.findViewById(R.id.plus_btn_2);
        plus_btn_3 = v.findViewById(R.id.plus_btn_3);
        brochure_btn = v.findViewById(R.id.brochure_btn);
        clinical_report_btn = v.findViewById(R.id.clinical_report_btn);
        bunny_lens_AF_btn = v.findViewById(R.id.bunny_lens_AF_btn);
        accuject_Pro_btn = v.findViewById(R.id.accuject_Pro_btn);
        list_icon = v.findViewById(R.id.list_icon);
        tableImage = v.findViewById(R.id.tableImage);
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        tech_specs_btn.setOnClickListener(this);
        plus_btn_1.setOnClickListener(this);
        plus_btn_2.setOnClickListener(this);
        plus_btn_3.setOnClickListener(this);
        brochure_btn.setOnClickListener(this);
        clinical_report_btn.setOnClickListener(this);
        bunny_lens_AF_btn.setOnClickListener(this);
        accuject_Pro_btn.setOnClickListener(this);
        upload_icon.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.upload_icon:
                SendByEmailDialog.newInstance(
                        Constants.PRODUCT_BROCHURE_BUNNY,
                        Constants.CLONICAL_EREPORT_BUNNY)
                        .show(masterActivity.getSupportFragmentManager(), "SendByEmailDialog");
                break;
            case R.id.brochure_btn:
                new OpenLocalPDF(getContext(), Constants.PRODUCT_BROCHURE_BUNNY).execute();
                break;
            case R.id.clinical_report_btn:
                new OpenLocalPDF(getContext(), Constants.CLONICAL_EREPORT_BUNNY).execute();
                break;
            case R.id.bunny_lens_AF_btn:
                masterActivity.showVideoFragment(Constants.BUNNY_AF_VIDEO);
                break;
            case R.id.accuject_Pro_btn:
                masterActivity.showVideoFragment(Constants.BUNNU_AF_MEDICAL_SYSTEM);
                break;
            case R.id.tech_specs_btn:
                if (showSpecs) {
                    showSpecs = false;
                    tech_specs_btn.setText(masterActivity.getString(R.string.back_txt));
                    html_TV.setVisibility(View.GONE);
                    tableImage.setVisibility(View.VISIBLE);
                } else {
                    showSpecs = true;
                    tech_specs_btn.setText(masterActivity.getString(R.string.tech_specs));
                    html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
                    html_TV.setVisibility(View.VISIBLE);
                    tableImage.setVisibility(View.GONE);
                }
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
            case R.id.plus_btn_1:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_BUNNY_LENS_AF_HOT_SPOT_2);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_2:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_BUNNY_LENS_AF_HOT_SPOT_1);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_3:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_BUNNY_LENS_AF_HOT_SPOT_3);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
        }

    }

}
