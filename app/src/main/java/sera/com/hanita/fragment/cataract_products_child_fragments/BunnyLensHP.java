package sera.com.hanita.fragment.cataract_products_child_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.dialog.dialog_fragment.CommonPopUpDialog;
import sera.com.hanita.dialog.dialog_fragment.SendByEmailDialog;
import sera.com.hanita.fragment.NotificationFragment;
import sera.com.hanita.utils.Constants;
import sera.com.hanita.utils.OpenLocalPDF;

/**
 * A simple {@link Fragment} subclass.
 */
public class BunnyLensHP extends Fragment implements View.OnClickListener {

    private TextView backBtn;
    private TextView html_TV;
    private MasterActivity masterActivity;
    private CommonPopUpDialog commonPopUpDialog;
    private ImageView plus_btn_1;
    private ImageView plus_btn_2;
    private ImageView plus_btn_3;
    private ImageView plus_btn_4;
    private ImageView plus_btn_5;
    private ImageView upload_icon;
    private ImageView envelope;
    private ImageView list_icon;
    private ImageView tableImage;
    private Button brochure_btn;
    private Button raw_material_btn;
    private TextView tech_specs_btn;
    private boolean showSpecs = true;
    private String htmlString;


    public BunnyLensHP() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_bunny_lens_hp, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();

        htmlString =
                "The  Aspheric  BunnyLens  HP  was  designed  using <br>"
                        + "the  most  advanced  tools  by  a  professional  R&D team  of  optical  and  mechanical  engineers.  The  optical  profile  of  the  BunnyLens  HP  was  calculated  using  ZEMAX™ software – a  simulating  tool  for  the optical  design  optimization."
                        + "Calculations  were  aimed  to  minimize  all aberrations,  including  the  spherical  aberration  of the  cornea,  and  to  optimize  the  MTF  (Modular Transfer  Function)<br><br>"
                        + "<font color='#d666a5'>Eye Model</font><br>"
                        + "The  Optical  design  of  BunnyLens  HP  was performed  using  the  advanced  Arizona  Eye  model.<br>"
                        + "[1] The  parameters  and  dimensions  of  the  eye  model  are  consistent  with  average  human  data.  The " +
                        "model  was  designed  to  match  clinical  levels  of  aberrations,  both  on  and  off  axis.  The  retina curvature  is  designed  to  split  the  tangential  and " +
                        "sagittal  foci  off-axis.  The  result  is  an  accurate  simulation  of  the  visual  performance  of  the " +
                        "BunnyLens  HP  in  the  Post-  operative  eye." +
                        "[1]  Field  Guide  to  Visual  and  Ophthalmic  Optics;" +
                        "Jim  Schwiegerling;  Nov  2004<br><br>"
                        + "<font color='#d666a5'>Geometrical Design</font><br>"
                        + "BunnyLens  HP  ensures  excellent  stability  and centration  due  to  the  4 – point  haptic  design." +
                        "360°  square  edge  in  order  to  minimize  PCO.<br><br>"
                        + "<font color='#d666a5'>Material</font><br>"
                        + "The  BunnyLens  HP  is  made  of  a  hydrophobic material  with  bio-adhesive  characteristics,  resulting  in  reduction  of  secondary  cataract procedures[1]." +
                        "The  BunnyLens  HP  has  a  very  low  glistening effect[2,3]." +
                        "The  BunnyLens  HP  hydrophobic  material incorporates  a  natural  yellow,  violet  filtering," +
                        "chromophore  for  better  protection  of  the  retina. <br><br>"
                        + "[1]  Advances  in  Intraocular  Lens  Materials  and  Designs:  Maximizing" +
                        "Biocompatibility  and  Optical  Performance;  T.P.  O’Brien,  Ophthalmologica  2003;217" +
                        "  [2]  Data  from  Benz  Research  and  development  QA  Dept" +
                        "  [3]  Glistenings  in  the  Single-Piece  Hydrophobic,  Acrylic" +
                        "Intraocular Lenses;  Aaron  Waite,  Nathan  Faulkner  and  Randall  J.Olson;  J Ophthalmol  2007;144:143-144"
                        + "</p>";

        html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
        return v;
    }

    private void initView(View v) {
        upload_icon = v.findViewById(R.id.upload_icon);

        backBtn = v.findViewById(R.id.backBtn);
        html_TV = v.findViewById(R.id.html_TV);
        plus_btn_1 = v.findViewById(R.id.plus_btn_1);
        plus_btn_2 = v.findViewById(R.id.plus_btn_2);
        plus_btn_3 = v.findViewById(R.id.plus_btn_3);
        plus_btn_4 = v.findViewById(R.id.plus_btn_4);
        plus_btn_5 = v.findViewById(R.id.plus_btn_5);
        brochure_btn = v.findViewById(R.id.brochure_btn);
        raw_material_btn = v.findViewById(R.id.raw_material_btn);
        envelope = v.findViewById(R.id.envelope);
        list_icon = v.findViewById(R.id.list_icon);
        tech_specs_btn = v.findViewById(R.id.tech_specs_btn);
        tableImage = v.findViewById(R.id.tableImage);
    }

    private void initListeners() {
        backBtn.setOnClickListener(this);
        plus_btn_1.setOnClickListener(this);
        plus_btn_2.setOnClickListener(this);
        plus_btn_3.setOnClickListener(this);
        plus_btn_4.setOnClickListener(this);
        plus_btn_5.setOnClickListener(this);
        brochure_btn.setOnClickListener(this);
        raw_material_btn.setOnClickListener(this);
        upload_icon.setOnClickListener(this);
        envelope.setOnClickListener(this);
        list_icon.setOnClickListener(this);
        tech_specs_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.upload_icon:
                SendByEmailDialog.newInstance(
                        Constants.BUNNY_HP_BROCHURE)
                        .show(masterActivity.getSupportFragmentManager(), "SendByEmailDialog");
                break;
            case R.id.brochure_btn:
                new OpenLocalPDF(getContext(), Constants.BUNNY_HP_BROCHURE).execute();
                break;
            case R.id.raw_material_btn:
                // add
                break;
            case R.id.tech_specs_btn:
                if (showSpecs) {
                    showSpecs = false;
                    tech_specs_btn.setText(masterActivity.getString(R.string.back_txt));
                    html_TV.setVisibility(View.GONE);
                    tableImage.setVisibility(View.VISIBLE);
                } else {
                    showSpecs = true;
                    tech_specs_btn.setText(masterActivity.getString(R.string.tech_specs));
                    html_TV.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
                    html_TV.setVisibility(View.VISIBLE);
                    tableImage.setVisibility(View.GONE);
                }
                break;
            case R.id.backBtn:
                masterActivity.doBackPress();
                break;
            case R.id.plus_btn_1:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_BUNNY_LENS_HP_HOT_SPOT_1);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_2:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_BUNNY_LENS_HP_HOT_SPOT_2);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_3:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_BUNNY_LENS_HP_HOT_SPOT_4);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_4:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_BUNNY_LENS_HP_HOT_SPOT_3);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
            case R.id.plus_btn_5:
                commonPopUpDialog = CommonPopUpDialog.newInstance(Constants.CATARACT_BUNNY_LENS_HP_HOT_SPOT_5);
                commonPopUpDialog.show(masterActivity.getSupportFragmentManager(), "CommonPopUpDialog");
                break;
        }


    }

}
