package sera.com.hanita.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.R;
import sera.com.hanita.adapter.MainPagerAdapter;


public class MainPagerFragment extends Fragment implements View.OnClickListener {

    private Toolbar toolbar;
    private ViewPager viewPager;
    private MasterActivity masterActivity;
    private ImageView list_icon;
    private ImageView envelope;

    public MainPagerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_main_pager, container, false);
        masterActivity = (MasterActivity) getActivity();
        initView(v);
        initListeners();
        setUpPager();
        return v;
    }

    private void setUpPager() {
        List<Drawable> list = new ArrayList<>();
        list.add(getResources().getDrawable(R.drawable.lens_instructions));
        list.add(getResources().getDrawable(R.drawable.company_profile));
        list.add(getResources().getDrawable(R.drawable.lens_catalog));
        list.add(getResources().getDrawable(R.drawable.publications));
        list.add(getResources().getDrawable(R.drawable.calcualtor));
        list.add(getResources().getDrawable(R.drawable.surgeries));
        viewPager.setAdapter(new MainPagerAdapter(masterActivity, list));
    }

    private void initListeners() {
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        list_icon.setOnClickListener(this);
        envelope.setOnClickListener(this);
    }

    private void initView(View v) {
        viewPager = v.findViewById(R.id.viewPager);
        toolbar = v.findViewById(R.id.toolbar);
        list_icon = v.findViewById(R.id.list_icon);
        envelope = v.findViewById(R.id.envelope);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.envelope:
                masterActivity.showChildFragment(new NotificationFragment());
                break;
            case R.id.list_icon:
                masterActivity.openCloseDrawer();
                break;
        }
    }
}
