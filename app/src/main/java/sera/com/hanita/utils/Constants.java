package sera.com.hanita.utils;

import sera.com.hanita.R;

public class Constants {

    public static final int NOTIFICATION_MODE = 0;
    public static final int PUBLICATION_MODE = 1;

    public static final int HYDROPHONIC = 0;
    public static final int SPHERIC_IOLS = 1;
    public static final int ASPHERIC_IOLS = 2;
    public static final int FULL_RANGE_IOLS = 3;
    public static final int TORIC_IOLS = 4;
    public static final int OVD = 5;
    public static final int ACCESSORIES = 6;
    public static final int MISCELLANEOUS = 7;
    public static final int HP_EASY = 8;


    public static final int SOFTJECT = 0;
    public static final int ACCUJET = 1;
    public static final int SEE_LENS = 2;
    public static final int BUNNY_LENS = 3;

    public static final String WEB_VIEW_CALCULATOR_URL = "http://www.hanitalenses.com/toric-iol-calculator-v5-01/";

    // Exo video
    public static final String SOFTJECT_2_4_BIG = "instructions_Softject.mp4";
    public static final String ACCUJET_PRELOADED = "accuject_preloaded.mp4";
    public static final String COMPANY_PROFILE_SLIDE_1 = "company_profile_slide_1.mp4";
    public static final String COMPANY_PROFILE_SLIDE_2 = "company_slide.mp4";

    public static final String BUNNY_AF_VIDEO = "seelens_video.mp4";

    public static final String BUNNU_AF_MEDICAL_SYSTEM = "medicel_preloaded_system.mp4";
    public static final String SEE_LENS_AF_MEDICAL_SYSTEM = "medical_accujet_see_lens_af.mp4";
    public static final String VIS_TOR_VIDEO = "vis_tor_video.mp4";

    public static final String SEE_LENS_AF = "seelens_video.mp4";

    public static final String HP_EASY_BUNNY_LENS = "hp_easy_bunny_lens.mp4";
    public static final String HP_EASY_SEE_LENS = "hp_easy_see_lens.mp4";


    // surgeries video
    public static final String SEE_LENS_MF_PAPPARO = "see_lens_mf_papparo.mp4";
    public static final String SEE_LENS_HP_BELUCCI = "see_lens_hp_belucci.mp4";
    public static final String SEE_LENS_HP_SAVARESI = "see_lens_hp_savaresi.mp4";
    public static final String SEE_LENS_AF_PANDEY = "see_lens_af_pandey.mp4";
    public static final String SEE_LENS_SHASHKI = "see_lens_shashki.mp4";
    public static final String ASSI_ANCHOR = "assi_anchor.mp4";
    public static final String VIS_TOR_SURG = "vis_tor_surg.mp4";

    // common popup dialog layouts
    public static final int CATARACT_VISTOR_HOT_SPOT_1 = R.layout.vis_tor_hot_spot_1;
    public static final int CATARACT_VISTOR_HOT_SPOT_2 = R.layout.vis_tor_hot_spot_2;
    public static final int CATARACT_VISTOR_HOT_SPOT_3 = R.layout.vis_tor_hot_spot_3;
    public static final int CATARACT_VISTOR_HOT_SPOT_4 = R.layout.vis_tor_hot_spot_4;
    public static final int CATARACT_VISTOR_HOT_SPOT_5 = R.layout.vis_tor_hot_spot_5;

    public static final int CATARACT_ASSI_HOT_SPOT_1 = R.layout.assi_hot_spot_1;
    public static final int CATARACT_ASSI_HOT_SPOT_2 = R.layout.assi_hot_spot_2;
    public static final int CATARACT_ASSI_HOT_SPOT_3 = R.layout.assi_hot_spot_3;
    public static final int CATARACT_ASSI_HOT_SPOT_4 = R.layout.assi_hot_spot_4;
    public static final int CATARACT_ASSI_HOT_SPOT_5 = R.layout.assi_hot_spot_5;

    public static final int CATARACT_SEE_LENNS_HP__HOT_SPOT_1 = R.layout.see_lens_hp_hot_spot_1;
    public static final int CATARACT_SEE_LENNS_HP__HOT_SPOT_2 = R.layout.see_lens_hp_hot_spot_2;
    public static final int CATARACT_SEE_LENNS_HP__HOT_SPOT_3 = R.layout.see_lens_hp_hot_spot_3;
    public static final int CATARACT_SEE_LENNS_HP__HOT_SPOT_4 = R.layout.see_lens_hp_hot_spot_5;
    public static final int CATARACT_SEE_LENNS_HP__HOT_SPOT_5 = R.layout.see_lens_hp_hot_spot_4;

    public static final int CATARACT_SEE_LENNS_AF_HOT_SPOT_1 = R.layout.see_lens_af_hot_spot_2;
    public static final int CATARACT_SEE_LENNS_AF_HOT_SPOT_2 = R.layout.see_lens_af_hot_spot_1;
    public static final int CATARACT_SEE_LENNS_AF_HOT_SPOT_3 = R.layout.see_lens_af_hot_spot_3;
    public static final int CATARACT_SEE_LENNS_AF_HOT_SPOT_4 = R.layout.see_lens_af_hot_spot_4;
    public static final int CATARACT_SEE_LENNS_AF_HOT_SPOT_5 = R.layout.see_lens_af_hot_spot_5;

    public static final int CATARACT_B_LENNS_SPOT_1 = R.layout.b_lens_hot_spot_1;
    public static final int CATARACT_B_LENNS_SPOT_2 = R.layout.b_lens_hot_spot_2;
    public static final int CATARACT_B_LENNS_SPOT_3 = R.layout.b_lens_hot_spot_3;

    public static final int CATARACT_BUNNY_LENS_AF_HOT_SPOT_1 = R.layout.bunny_lens_1;
    public static final int CATARACT_BUNNY_LENS_AF_HOT_SPOT_2 = R.layout.bunny_lens_2;
    public static final int CATARACT_BUNNY_LENS_AF_HOT_SPOT_3 = R.layout.bunny_lens_3;

    public static final int CATARACT_BUNNY_LENS_HP_HOT_SPOT_1 = R.layout.bunny_lens_hp_1;
    public static final int CATARACT_BUNNY_LENS_HP_HOT_SPOT_2 = R.layout.bunny_lens_hp_2;
    public static final int CATARACT_BUNNY_LENS_HP_HOT_SPOT_3 = R.layout.bunny_lens_hp_3;
    public static final int CATARACT_BUNNY_LENS_HP_HOT_SPOT_4 = R.layout.bunny_lens_hp_5;
    public static final int CATARACT_BUNNY_LENS_HP_HOT_SPOT_5 = R.layout.bunny_lens_hp_4;


    public static final int CATARACT_FULL_RANGE_HOT_SPOT_1 = R.layout.full_range_1;
    public static final int CATARACT_FULL_RANGE_HOT_SPOT_2 = R.layout.full_range_2;
    public static final int CATARACT_FULL_RANGE_HOT_SPOT_3 = R.layout.full_range_3;
    public static final int CATARACT_FULL_RANGE_HOT_SPOT_4 = R.layout.full_range_4;
    public static final int CATARACT_FULL_RANGE_HOT_SPOT_5 = R.layout.full_range_5;
    public static final int CATARACT_FULL_RANGE_HOT_SPOT_6 = R.layout.full_range_6;

    public static final int CATARACT_SEE_LENS_HOT_SPOT_1 = R.layout.see_lens_1;
    public static final int CATARACT_SEE_LENS_HOT_SPOT_2 = R.layout.see_lens_2;
    public static final int CATARACT_SEE_LENS_HOT_SPOT_3 = R.layout.see_lens_3;

    public static final int HP_EASY_HOT_SPOT_1 = R.layout.hp_easy_geometrical;
    public static final int HP_EASY_HOT_SPOT_2 = R.layout.hp_easy_refra;
    public static final int HP_EASY_HOT_SPOT_3 = R.layout.hp_easy_material;
    public static final int HP_EASY_HOT_SPOT_4 = R.layout.hp_easy_prelo;
    public static final int HP_EASY_HOT_SPOT_5 = R.layout.hp_easy_geometrical;
    public static final int HP_EASY_HOT_SPOT_6 = R.layout.hp_easy_refra_bu;

    // PDF reference
    public static final String PRODUCT_BROCHURE_ASSI = "Product_Brochure_assi";
    public static final String ZONULAR_ASSI = "Compromised_Zonular_Support_assi";

    public static final String PRODUCT_BROCHURE_BLENS = "B_LENS_brochure";
    public static final String QA_BLENS = "Clinical_QA_feedback_Data_blens";
    public static final String CLONICAL_EXP = "Clinical_Experience_blens";
    public static final String CLONICAL_EXP_1Y = "Clinical_Experience_1Y_blens";

    public static final String PRODUCT_BROCHURE_BUNNY = "Product_Brochure_bunny";
    public static final String CLONICAL_EREPORT_BUNNY = "Clinical_Evaluation_bunny";

    public static final String BUNNY_HP_BROCHURE = "bunny_hp_product";

    public static final String VIS_TOR_BROCHURE = "vis_tor_broshure";
    public static final String VIS_TOR_SLIDE = "vis_tor_slide";

    public static final String FUUL_RANGE_BROCHURE = "company_brochure_fullrange_print";
    public static final String FULL_RANGE = "full_range"; // changed with FUUL_RANGE_BROCHURE 6/11/18

    public static final String VISCO_1_4_BROCHURE = "Visco1.4_Brochure";
    public static final String VISCO_1_8_BROCHURE = "Visco1.8_Brochure";

    public static final String SEE_LENS_CLINICAL = "Clinical_Evaluation";
    public static final String SEE_LENS_CLINICAL_2_Y = "Clinical_Evaluation2";
    public static final String SEE_LENS_BROCHURE = "Seelens_brochure";

    public static final String PERFECTOR_BROCURE = "perfector_brocure";
    public static final String PERFECTOR_PROFESSOR = "roberto_belicchi";

    public static final String SEE_LENS_AF_CLINICAL = "Clinical_Evaluation_see_lens_af";
    public static final String SEE_LENS_AF_BROCHURE = "Seelens_AF_Product_Brochure";

    public static final String SEE_LENS_HP_BROCHURE = "Product_Brochure_see_lens_hp";
    public static final String SEE_LENS_HP_CLINICAL = "Clinical_Evaluation_see_lens_hp";
    public static final String SEE_LENS_HP_RAW_MATERIAL = "raw_material";

    public static final String SOFT_JECT_BROCHURE_1_8 = "brochure_soft_1.8";
    public static final String SOFT_JECT_BROCHURE_2_4 = "brochure_soft_2.4";
    public static final String SOFT_JECT_INSTRUCTIONS_1_8 = "instruction_1.8";
    public static final String SOFT_JECT_INSTRUCTIONS_2_4 = "instruction_2.4";

    public static final String PUBLICATION_0 = "public_0";
    public static final String PUBLICATION_1 = "public_1";
    public static final String PUBLICATION_2 = "public_2";
    public static final String PUBLICATION_3 = "public_3";

    public static final String ALL_BROCHURE_PDF = "all_brochures_pdf_file";

    public static final String CONSTANT_TABLE_PDF = "constantTable";

    public static final String HP_EASY_BROCHURE = "hp_easy_brochure";
}
