package sera.com.hanita.utils;

public interface CallBack {
    void onItemClicked(Integer position);
}
