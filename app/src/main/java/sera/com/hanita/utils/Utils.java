package sera.com.hanita.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import sera.com.hanita.R;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class Utils {

    public static String DIRECTORYNAME = "/Hanita/";

    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo ni = Objects.requireNonNull(cm).getActiveNetworkInfo();

        if (ni != null) {
            return ni.isConnected();
        }

        return false;
    }

    public static List<Drawable> getImageList(Context context) {
        List<Drawable> result = new ArrayList<>();
        result.add(context.getResources().getDrawable(R.drawable.hydrophobic));
        result.add(context.getResources().getDrawable(R.drawable.spheric));
        result.add(context.getResources().getDrawable(R.drawable.aspheric));
        result.add(context.getResources().getDrawable(R.drawable.fullrange));
        result.add(context.getResources().getDrawable(R.drawable.toric));
        result.add(context.getResources().getDrawable(R.drawable.ovd));
        result.add(context.getDrawable(R.drawable.accessories));
        result.add(context.getResources().getDrawable(R.drawable.miscellaneous));
//        result.add(context.getResources().getDrawable(R.drawable.hp_easy_bckg));
        result.add(context.getResources().getDrawable(R.drawable.constant));
        return result;
    }

//    public static List<String> getStringList(Context context) {
//        List<String> itemList = new ArrayList<>();
//        itemList.add(context.getString(R.string.side_list_0));
//        itemList.add(context.getString(R.string.side_list_1));
//        itemList.add(context.getString(R.string.side_list_2));
//        itemList.add(context.getString(R.string.side_list_3));
//        itemList.add(context.getString(R.string.side_list_4));
//        itemList.add(context.getString(R.string.side_list_5));
//        itemList.add(context.getString(R.string.side_list_6));
//        itemList.add(context.getString(R.string.side_list_7));
//        itemList.add(context.getString(R.string.constant_table));
//        return itemList;
//    }
//
//    public static List<Drawable> getPublicationsImagesList(Context context) {
//        List<Drawable> result = new ArrayList<>();
//        result.add(context.getResources().getDrawable(R.drawable.public_0));
//        result.add(context.getResources().getDrawable(R.drawable.public_1));
//        result.add(context.getResources().getDrawable(R.drawable.public_2));
//        result.add(context.getResources().getDrawable(R.drawable.public_3));
//        return result;
//    }


}
