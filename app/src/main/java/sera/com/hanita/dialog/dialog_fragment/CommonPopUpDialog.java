package sera.com.hanita.dialog.dialog_fragment;

import android.app.Dialog;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import java.util.Objects;

import sera.com.hanita.R;

public class CommonPopUpDialog extends DialogFragment {

    private TextView closeTV;
    private TextView text_TV_Blens_hot_spot_2;
    private int resource;

    public CommonPopUpDialog() {
    }

    public static CommonPopUpDialog newInstance(int resource) {
        CommonPopUpDialog dialog = new CommonPopUpDialog();
        Bundle args = new Bundle();
        args.putInt("resource", resource);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
        setCancelable(false);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog d = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(d.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        d.getWindow().setWindowAnimations(R.style.ConfirmAnimation);
        return d;
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            Window window = getDialog().getWindow();
            Point size = new Point();
            Display display = Objects.requireNonNull(window).getWindowManager().getDefaultDisplay();
            display.getSize(size);
            int width = size.x;
            int height = size.y;
            window.setLayout((int) (width * 0.9), (int) (height * 0.9));
            window.setGravity(Gravity.CENTER);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        resource = Objects.requireNonNull(getArguments()).getInt("resource");
        View view = inflater.inflate(resource, container, false);
        initView(view);
        setListener();
        setUpHtml();
        return view;
    }

    private void setUpHtml() {
        if (text_TV_Blens_hot_spot_2 != null) {
            String htmlString =
                    "<p><h3><font color='black'>CLINICAL EXPERIENCE WITH HYDROPHILIC ACRYLIC LENSES.</font></h3><br>"
                            + "Ehud I. Assia, MD,<br>"
                            + "Fani Segev MD<br>"
                            + "Yoanis ZacharopoulosMD,<br>"
                            + "Simona Jaeger-Roshu MD,<br><br>"
                            + "Department of Ophthalmology,<br>"
                            + "Meir Hospital, Sapir Medical Center,<br>"
                            + " Kfar-Saba, Israel<br><br>"
                            + "<font color='black'>Blens Implanted</font> – over 1000 IOLs Report - 100 patients <br>"
                            + "Follow up – one year<br><br>"
                            + "<font color='black'> Blens Study Conclusions:</font><br>"
                            + "“Very good optical performance”<br>"
                            + "“Low rate of clinical PCO”<br>"
                            + "“Excellent centration”<br></p>";

            text_TV_Blens_hot_spot_2.setText(Html.fromHtml(htmlString), TextView.BufferType.SPANNABLE);
        }
    }

    private void setListener() {
        closeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void initView(View view) {
        closeTV = view.findViewById(R.id.closeTV);
        text_TV_Blens_hot_spot_2 = view.findViewById(R.id.text_TV_Blens_hot_spot_2);
    }

}
