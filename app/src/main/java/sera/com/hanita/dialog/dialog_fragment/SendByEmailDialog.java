package sera.com.hanita.dialog.dialog_fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Objects;

import sera.com.hanita.R;
import sera.com.hanita.activity.MasterActivity;
import sera.com.hanita.api.SendPdfTask;
import sera.com.hanita.utils.Constants;

import static sera.com.hanita.utils.Constants.ALL_BROCHURE_PDF;

public class SendByEmailDialog extends android.support.v4.app.DialogFragment implements View.OnClickListener {

    private LinearLayout sendCurrent;
    private LinearLayout sendAll;
    private MasterActivity masterActivity;
    private TextView closeTV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
        setCancelable(false);
    }


    public static SendByEmailDialog newInstance(String... strings) {
        SendByEmailDialog dialog = new SendByEmailDialog();
        Bundle args = new Bundle();
        args.putStringArray("array", strings);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog d = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(d.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        d.getWindow().setWindowAnimations(R.style.PopUpAnimation);
        return d;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.send_by_email_layout, container, false);
        iniView(v);
        setListener();
        masterActivity = (MasterActivity) getActivity();
        return v;
    }

    private void setListener() {
        sendCurrent.setOnClickListener(this);
        closeTV.setOnClickListener(this);
        sendAll.setOnClickListener(this);
    }

    private void iniView(View v) {
        sendCurrent = v.findViewById(R.id.sendCurrent);
        sendAll = v.findViewById(R.id.sendAll);
        closeTV = v.findViewById(R.id.closeTV);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendCurrent:
                new SendPdfTask(masterActivity).execute(Objects.requireNonNull(getArguments()).getStringArray("array"));
                break;
            case R.id.sendAll:
                new SendPdfTask(masterActivity).execute(ALL_BROCHURE_PDF);
                break;
            case R.id.closeTV:
                dismiss();
                break;
        }
    }
}
