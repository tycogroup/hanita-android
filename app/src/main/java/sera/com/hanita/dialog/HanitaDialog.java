package sera.com.hanita.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Objects;

import sera.com.hanita.R;

public class HanitaDialog extends android.support.v4.app.DialogFragment {

    private TextView alertTV;
    private TextView btnOk;
    private TextView btnCancel;
    private TextView top_bar_txt;
    public static final int ALERT = 1;
    public static final int INFO = 2;
    private RelativeLayout top_bar;
    private LinearLayout canceLL;

    private HanitaDialogDismissListener mListener = null;

    public void setListener(HanitaDialogDismissListener mListener) {
        this.mListener = mListener;
    }

    public HanitaDialog() {

    }

    public static HanitaDialog newInstance(String message, boolean isCancel, int status) {

        HanitaDialog d = new HanitaDialog();

        Bundle args = new Bundle();
        args.putInt("status", status);
        args.putString("message", message);
        args.putBoolean("isCancel", isCancel);
        d.setArguments(args);

        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
        setCancelable(false);
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog d = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(d.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        d.getWindow().setWindowAnimations(R.style.PopUpAnimation);
        return d;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.alert_dialog_view, container, false);

        alertTV = v.findViewById(R.id.alertTV);
        btnOk = v.findViewById(R.id.btnOk);
        btnCancel = v.findViewById(R.id.btnCancel);
        top_bar = v.findViewById(R.id.top_bar);
        top_bar_txt = v.findViewById(R.id.top_bar_txt);
        canceLL = v.findViewById(R.id.canceLL);
        top_bar.setBackground(getBackColor());
        alertTV.setText(getArguments().getString("message"));

        canceLL.setVisibility(getArguments().getBoolean("isCancel") ? View.VISIBLE : View.GONE);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onDismiss();
                }
                dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return v;
    }

    private Drawable getBackColor() {
        switch (getArguments().getInt("status")) {
            case ALERT:
                top_bar_txt.setText("ALERT");
                return getResources().getDrawable(R.drawable.bg_corners_round_dark_red);
            //return getResources().getColor(R.color.dark_red);

            case INFO:
                top_bar_txt.setText("NOTICE");
                return getResources().getDrawable(R.drawable.bg_cornet_round_blue);
            //return getResources().getColor(R.color.company_txt_color);

        }
        return null;
    }

    public interface HanitaDialogDismissListener {

        void onDismiss();
    }
}
